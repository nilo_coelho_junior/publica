
Rails.application.routes.draw do

  devise_for :users, path: "auth", path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret', confirmation: 'verification', unlock: 'unblock', registration: 'register', sign_up: 'sign_up' }

  namespace :admin do
    root 'events#index'
    resources :events, path: "eventos" do
      get "report"
      collection do 
        get "search"
      end
      resources :researches, path: "pesquisas" do
        get "report"
        resources :identifiers, path: "identificadores"
        resources :surveys, path: "questionarios", param: :number_survey do
            match 'answers' => 'answers#new', via: :get, path: "respostas"
            match 'answers' => 'answers#create', via: :post, path: "respostas"
            match 'answers' => 'answers#update', via: :patch, path: "respostas"
            match 'answers' => 'answers#update', via: :put, path: "respostas"
            
            collection do
              resources :survey_participants, path: 'participantes'
              match 'import' => 'import_surveys#index',  via: :get, path: "importar"
              match 'import' => 'import_surveys#import', via: :post, path: "importar"
            end
        end
        member do
          match 'groups' => 'groups#index', via: :get, path: "perguntas"
          match 'groups' => 'groups#create', via: :post, path: "perguntas"
          match 'groups' => 'groups#update', via: :patch, path: "perguntas"
          match 'groups' => 'groups#update', via: :put, path: "perguntas"
        end
      end 
    end
    resources :users, path: "usuarios"
  end

  namespace :ajax do 
    get "report", to: "report#index"
    get "identifiers", to: "report#identifiers"
  end

  root 'home#index'
  
end
