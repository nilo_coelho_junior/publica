# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(name: "Nilo", email: "nilocoelhojunior@gmail.com", password: "12345678", role: "Administrador")
User.create(name: "Joao", email: "joao@email.com", password: "12345678", role: "Coordenador")

Event.create(title: "Evento de Teste", status: 1, author_id: 1)
Research.create(title: "Pesquisa de Teste", author_id: 1, status: 1, event_id: 1)

