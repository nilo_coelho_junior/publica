class CreateSurveyIdentifiers < ActiveRecord::Migration
  def change
    create_table :survey_identifiers do |t|
      t.belongs_to :survey
      t.belongs_to :identifier
      t.string :value

      t.timestamps
    end
    add_index(:survey_identifiers, [:survey_id, :identifier_id, :value], :unique => true, :name => 'survey_identifier')
  end
end
