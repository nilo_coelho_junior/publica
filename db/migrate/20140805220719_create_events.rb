class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.string :description
      t.integer :status, default: 1
      t.string :author_id

      t.timestamps
    end
  end
end
