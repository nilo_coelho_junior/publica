class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.belongs_to :survey
      t.belongs_to :question
      t.references :alternative
      t.text :value
      t.integer :order

      t.timestamps
    end
  end
end
