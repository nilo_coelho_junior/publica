class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :title
      t.references :research, index: true

      t.timestamps
    end
  end
end
