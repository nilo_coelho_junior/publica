class CreateSurveys < ActiveRecord::Migration
  def change
    create_table :surveys do |t|
      t.references :research, index: true
      t.string :number_survey
      t.belongs_to :participant
      t.belongs_to :author
      t.integer :status, default: 1
      t.timestamps
    end
  end
end
