class CreateAlternatives < ActiveRecord::Migration
  def change
    create_table :alternatives do |t|
      t.string :title
      t.references :question, index: true
      t.integer :as_other
      t.timestamps
    end
  end
end
