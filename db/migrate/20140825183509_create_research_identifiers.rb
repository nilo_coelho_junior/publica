class CreateResearchIdentifiers < ActiveRecord::Migration
  def change
    create_table :research_identifiers do |t|
      t.integer :research_id
      t.integer :identifier_id

      t.timestamps
    end
  end
end
