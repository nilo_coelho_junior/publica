class CreateResearches < ActiveRecord::Migration
  def change
    create_table :researches do |t|
      t.string :title
      t.string :description
      t.belongs_to :author
      t.belongs_to :event
      t.integer :status, default: 1

      t.timestamps
    end
  end
end
