class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :title
      t.integer :type_question
      t.references :group, index: true
      t.integer :father_id, index: true
      t.timestamps
    end
  end
end
