json.array!(@answers) do |answer|
  json.extract! answer, :id, :research, :question_id, :objective, :subjective, :author
  json.url answer_url(answer, format: :json)
end
