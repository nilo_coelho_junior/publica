json.array!(@questions) do |question|
  json.extract! question, :id, :topic, :title, :type_question, :group_id
  json.url question_url(question, format: :json)
end
