json.array!(@identifiers) do |identifier|
  json.extract! identifier, :id, :name, :type
  json.url identifier_url(identifier, format: :json)
end
