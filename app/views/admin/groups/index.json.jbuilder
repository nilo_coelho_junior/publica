json.array!(@groups) do |group|
  json.extract! group, :id, :topic, :title, :research_id
  json.url group_url(group, format: :json)
end
