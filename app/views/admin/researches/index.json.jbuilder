json.array!(@researches) do |research|
  json.extract! research, :id, :title, :description, :date_start, :date_end, :status, :author, :researchers
  json.url research_url(research, format: :json)
end
