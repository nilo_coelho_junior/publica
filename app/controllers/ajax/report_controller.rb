module Ajax
	class ReportController < AjaxController
		def index
			de, ate = [], []
			
			if params[:data]
				params[:data].each{|index, value|
					if !value[:value].empty?
						if value[:name] == "de"
							de << value[:value]
						else
							ate << value[:value]
						end
					end
				}
			end

			de = de.join("-")
			ate = ate.join("-")
			
			if de.empty? || ate.empty?
				@report = Research.joins("inner join (select groups.id, groups.research_id from groups where groups.research_id = #{params[:research]} order by groups.id) as groups on groups.research_id = researches.id inner join (select questions.id, questions.group_id, questions.type_question from questions order by questions.id) as questions on questions.group_id = groups.id inner join (select alternatives.id, alternatives.title, alternatives.question_id from alternatives order by alternatives.id) as alternatives on alternatives.question_id = questions.id inner join (select answers.id, answers.question_id, answers.alternative_id from answers order by alternative_id) as answers on answers.alternative_id = alternatives.id").where("researches.id = ? and questions.type_question <> 0", params[:research]).select("questions.id as question_id, questions.type_question as question_type, alternatives.id, alternatives.title as title, count(answers.alternative_id) as total").group("groups.id, alternatives.title, question_type, questions.id, alternatives.id").order("groups.id, questions.id, alternatives.id")
			elsif de.to_i == 0 || ate.to_i == 0
				@report = Research.joins("inner join (select groups.id, groups.research_id from groups where groups.research_id = #{params[:research]} order by groups.id) as groups on groups.research_id = researches.id inner join (select questions.id, questions.group_id, questions.type_question from questions order by questions.id) as questions on questions.group_id = groups.id inner join (select alternatives.id, alternatives.title, alternatives.question_id from alternatives order by alternatives.id) as alternatives on alternatives.question_id = questions.id inner join (select answers.id, answers.question_id, answers.alternative_id, answers.survey_id from answers order by alternative_id) as answers on answers.alternative_id = alternatives.id").where("researches.id = ? and questions.type_question <> 0 and answers.survey_id IN(select surveys.id from surveys where surveys.research_id = ? and surveys.number_survey between ? and ?)", params[:research], params[:research], "%#{de}%", "%#{ate}%").select("questions.id as question_id, questions.type_question as question_type, alternatives.id, alternatives.title as title, count(answers.alternative_id) as total").group("groups.id, question_type, alternatives.title, questions.id, alternatives.id").order("groups.id, questions.id, alternatives.id")
			else
				@report = Research.joins("inner join (select groups.id, groups.research_id from groups where groups.research_id = #{params[:research]} order by groups.id) as groups on groups.research_id = researches.id inner join (select questions.id, questions.group_id, questions.type_question from questions order by questions.id) as questions on questions.group_id = groups.id inner join (select alternatives.id, alternatives.title, alternatives.question_id from alternatives order by alternatives.id) as alternatives on alternatives.question_id = questions.id inner join (select answers.id, answers.question_id, answers.alternative_id, answers.survey_id from answers order by alternative_id) as answers on answers.alternative_id = alternatives.id").where("researches.id = ? and questions.type_question <> 0 and answers.survey_id IN(select surveys.id from surveys where surveys.research_id = ? and (surveys.number_survey)::INT between ? and ?)", params[:research], params[:research], de, ate).select("questions.id as question_id, questions.type_question as question_type, alternatives.id, alternatives.title as title, count(answers.alternative_id) as total").group("groups.id, question_type, alternatives.title, questions.id, alternatives.id").order("questions.id")
			end
			respond_with(@report)
		end

		def identifiers

			if Research.find(params[:research]).surveys.joins(:survey_identifiers).where("number_survey LIKE ? and survey_identifiers.identifier_id = ?", "%#{params[:value]}%", params[:id]).select("survey_identifiers.value").first.value.to_i == 0

				result = Research.find(params[:research]).surveys.joins(:survey_identifiers).where("number_survey LIKE ? and survey_identifiers.identifier_id = ?", "%#{params[:value]}%", params[:id]).select("survey_identifiers.identifier_id, survey_identifiers.value").order("survey_identifiers.value").group("survey_identifiers.value, survey_identifiers.identifier_id")
			else
				result = Research.find(params[:research]).surveys.joins(:survey_identifiers).where("number_survey LIKE ? and survey_identifiers.identifier_id = ?", "%#{params[:value]}%", params[:id]).select("survey_identifiers.identifier_id, survey_identifiers.value").order("(survey_identifiers.value)::INT").group("survey_identifiers.value, survey_identifiers.identifier_id")
			end

			respond_with(result)
		end
	end
end