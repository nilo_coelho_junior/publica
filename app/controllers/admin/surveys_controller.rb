class Admin::SurveysController < Admin::AdminController
	load_and_authorize_resource
	skip_load_resource :only => [:create]

	before_action :set_event_identifier_research, only: [:show, :edit, :new, :index, :create, :destroy, :update, :participants]
	before_action :set_survey, only: [:show, :edit, :update, :destroy, :participants]
	before_action :get_surveys, only: [:index, :create, :update]

	def index
		add_breadcrumb "Eventos", :admin_events_path
    add_breadcrumb "#{@event.title}", :admin_events_path
    add_breadcrumb "#{@research.title}", :admin_event_researches_path
    add_breadcrumb "Questionários", :admin_event_research_surveys_path

	end

	def show
	end

	def new
		add_breadcrumb "Eventos", :admin_events_path
    add_breadcrumb "#{@event.title}", :admin_events_path
    add_breadcrumb "#{@research.title}", :admin_event_researches_path
    add_breadcrumb "Questionários", :admin_event_research_surveys_path
    add_breadcrumb "Criar Questionário"
    
		@survey = Survey.new
		
		if !@research.identifiers.any?
			redirect_to admin_event_research_surveys_path(@event, @research), alert: 'Antes de criar um Questionário você deve criar um Identificador'
		end
	end

	def edit
		add_breadcrumb "Eventos", :admin_events_path
    add_breadcrumb "#{@event.title}", :admin_events_path
    add_breadcrumb "#{@research.title}", :admin_event_researches_path
    add_breadcrumb "Questionários", :admin_event_research_surveys_path
    add_breadcrumb "#{@survey.number_survey}", :admin_event_research_surveys_path
    add_breadcrumb "Editar", :edit_admin_event_research_survey_path
	end

	def create

		survey_identifiers = params[:survey][:identifiers]
		p "--------------#{survey_identifiers}"
		@survey = Survey.new
		survey_identifier = SurveyIdentifier.new

		@survey.id = Survey.last.id+1
		@survey.research_id = @research.id
		@survey.author_id = current_user.id
		number_survey = ""
		
		survey_identifiers.each_with_index do |item, index|
			if index == survey_identifiers.size-1
				number_survey << item[1]
			else
				number_survey << item[1]+"-"
			end			
		end
		
		@survey.number_survey = number_survey

		respond_to do |format|
			if @survey.save
				
				survey_identifiers.each_with_index do |item, index|
					SurveyIdentifier.create(id: SurveyIdentifier.last.id+1, survey: @survey, identifier_id: item[0].to_i, value: item[1])
				end
				if @surveys.respond_to? :num_pages
					format.html { redirect_to admin_event_research_surveys_path(@event, @research, page: @surveys.num_pages), notice: 'Questionário criado com sucesso' }
				else
					format.html { redirect_to admin_event_research_surveys_path(@event, @research, @surveys), notice: 'Questionário criado com sucesso' }
				end
			    format.json { render :show, status: :created, location: admin_event_research_survey_path(@event, @research, @survey) }
			else
				format.html { render :new }
        		format.json { render json: @surveys.errors, status: :unprocessable_entity }
		   	end
		end
		
	end

	# PATCH/PUT /answers/1
	# PATCH/PUT /answers/1.json
	def update
		survey_identifiers = params[:survey][:identifiers]
		number_survey = ""
		survey_identifiers.each_with_index do |item, index|
			if index == survey_identifiers.size-1
				number_survey << item[1]
			else
				number_survey << item[1]+"-"
			end
		end

		if @survey.update(number_survey: number_survey, author_id: current_user.id)
			survey_identifiers.each_with_index do |item, index|
				@survey.survey_identifiers.find_by(identifier_id: item[0]).update(value: item[1])
			end
		end

		respond_to do |format|
		  	format.html { redirect_to admin_event_research_surveys_path(@event, @research, page: get_page(@survey, @surveys)), notice: 'Questionário atualizado com sucesso' }
        format.json { render :show, status: :ok, location: @answer }
		end
	end

	def destroy
		@survey.destroy
		respond_to do |format|
	      format.html { redirect_to admin_event_research_surveys_path, notice: 'Questionário removido com sucesso' }
	      format.json { head :no_content }
	    end
	end

	private
		# Use callbacks to share common setup or constraints between actions.
	    def set_survey	    	
	    	@survey = @research.surveys.find_by(number_survey: params[:number_survey])	    	
	    end

	    # Never trust parameters from the scary internet, only allow the white list through.
	    def answer_params
	      params.require(:survey).permit(identifiers: [])
	    end

	    def set_event_identifier_research
	      @event = Event.where(status: true).find(params[:event_id])
      	 @research = Research.find(params[:research_id])
	    end

	    def get_surveys
	    	if current_user.role == "Pesquisador"
					surveys = Survey.for_tasker(@research.id, current_user)
				else
					surveys = Survey.by_research(@research.id)
				end
				
				
				if !surveys.empty?
					if params[:search]
						p "search"
						if !params[:search].empty?
							p "search not empty"
							if !/\W/.match(surveys.first.number_survey)
								p "search int"
					      surveys = surveys.where("(number_survey)::INT = ?", params[:search])
					    else
					    	p "search text"
					      surveys = surveys.where("number_survey like ?", "%#{params[:search]}%")
					    end
				  	end
				  end

					if params[:de] or params[:ate]
						p "de ate"
						if !params[:de].empty? or !params[:ate].empty?
							p "de ate not empty"
							if !/\W/.match(surveys.first.number_survey)
								p "de ate int"
								if params[:ate].empty? 
									p "ate empty"
									p params[:de]
					      	surveys = surveys.where("(number_survey)::INT >= ?", params[:de])
					    	elsif params[:de].empty?
					    		p "de empty"
					    		surveys = surveys.where("(number_survey)::INT <= ?", params[:ate])
					      else
					      	p "de ate both empty"
					      	surveys = surveys.where("(number_survey)::INT between ? and ?", params[:de], params[:ate])
					      end
					    else
					    	p "de ate text"
					    	if params[:ate].empty?
					    		p "ate empty"
					    		surveys = surveys.where("number_survey >= ?", params[:de])
					    	elsif params[:de].empty?
					    		p "de empty"
					    		surveys = surveys.where("number_survey <= ?", params[:ate])
					    	else
					    		p "de ate both empty"
					      	surveys = surveys.where("number_survey between ? and ?", params[:de], params[:ate])
					    	end
					    end
				  	end
					end
					
					if params[:sort]
						if params[:sort].empty? or params[:sort] == "number_survey"
							if !/\W/.match(surveys.first.number_survey)
								@surveys = surveys.order("(number_survey)::INT").page params[:page]
							else
								@surveys = surveys.order("number_survey").page params[:page]
							end			
						elsif params[:sort] != "updated_at" and !/\W/.match(surveys.first.number_survey)
							@surveys = surveys.order("(#{params[:sort]})::INT").page params[:page]
						else
							@surveys = surveys.order("#{params[:sort]} DESC").page params[:page]
						end
					else
						if !/\W/.match(surveys.first.number_survey)
							@surveys = surveys.order("(number_survey)::INT").page params[:page]
						else
							@surveys = surveys.order("number_survey").page params[:page]
						end
					end
					@count = surveys.count
				else
					@surveys = surveys
				end
	    end
end