class Admin::QuestionListsController < Admin::AdminController
	authorize_resource :class => Admin::QuestionListsController

	before_action :set_question, only: [:show, :edit, :destroy, :update]
	before_action :set_event_research_group_question, only: [:show, :edit, :new, :index, :create, :destroy, :update]
	
	def index
		@questions = Question.where(group_id: @group, father_id: @question_father).order(:id)
	end

	def show
	end

	def new
		@question = Question.new
	end

	def edit
	end

	def create
		@question = Question.new(question_lists_params)
		@question.group_id = @group.id
		@question.father_id = @question_father.id
		
		respond_to do |format|
			if @question.save
				format.html { redirect_to admin_event_research_group_question_question_list_path(@event, @research, @group, @question_father, @question), notice: 'Question was successfully created.' }
        		format.json { render :show, status: :created, location: @question }
			else
				format.html { render :new }
        		format.json { render json: @question.errors, status: :unprocessable_entity }
			end
		end
	end

	def update
		respond_to do |format|
	      if @question.update(question_lists_params)
	        format.html { redirect_to admin_event_research_group_question_question_list_path(@event, @research, @group, @question_father, @question), notice: 'Question was successfully updated.' }
	        format.json { render :show, status: :ok, location: @identifier }
	      else
	        format.html { render :edit }
	        format.json { render json: @question.errors, status: :unprocessable_entity }
	      end
	    end
	end

	def destroy
		@question.destroy
		respond_to do |format|
	      format.html { redirect_to admin_event_research_group_question_question_lists_path(@event, @research, @group, @question_father), notice: 'Question was successfully destroyed.' }
	      format.json { head :no_content }
	    end
	end

	private

		def set_question
			@question = Question.find(params[:id])			
		end

	    def question_lists_params
	      params.require(:question_lists).permit(:title, :type_question, :group_id, :quantity, :father_id)
	    end

		def set_event_research_group_question
			@event = Event.where(status: true).find(params[:event_id])
      		@research = Research.where(status: true).find(params[:research_id])
      		@group = Group.find(params[:group_id])
      		@question_father = Question.find(params[:question_id])
		end
end
