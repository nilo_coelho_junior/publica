class Admin::QuestionsController < Admin::AdminController
  load_and_authorize_resource
  
  before_action :set_question, only: [:show, :edit, :update, :destroy]
  before_action :set_event_researche_group, only: [:show, :edit, :new, :index, :create, :destroy, :update]

  # GET /questions
  # GET /questions.json
  def index
    add_breadcrumb "Eventos", :admin_events_path
    add_breadcrumb "#{@event.title}", :admin_events_path
    add_breadcrumb "#{@research.title}", :admin_event_researches_path
    add_breadcrumb "Perguntas", :admin_event_research_questions_path
    
  end

  # GET /questions/1
  # GET /questions/1.json
  def show
  end

  # GET /questions/new
  def new
    @question = Question.new
  end

  # GET /questions/1/edit
  def edit
  end

  # POST /questions
  # POST /questions.json
  def create
    @question = Question.new(question_params)

    respond_to do |format|
      if @question.save
        format.html { redirect_to admin_event_research_group_question_path(@event, @research, @group, @question), notice: 'Question was successfully created.' }
        format.json { render :show, status: :created, location: @question }
      else
        format.html { render :new }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /questions/1
  # PATCH/PUT /questions/1.json
  def update
    respond_to do |format|
      if @question.update(question_params)
        format.html { redirect_to admin_event_research_group_question_path(@event, @research, @group, @question), notice: 'Question was successfully updated.' }
        format.json { render :show, status: :ok, location: @question }
      else
        format.html { render :edit }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /questions/1
  # DELETE /questions/1.json
  def destroy
    @question.destroy
    respond_to do |format|
      format.html { redirect_to admin_event_research_group_questions_path, notice: 'Question was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_question
      #@question = Question.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def question_params
      params.require(:question).permit(:topic, :title, :type_question, :group_id, :quantity)
    end

    def set_event_researche_group
      @event = Event.where(status: true).find(params[:event_id])
      @research = Research.where(status: true).find(params[:research_id])
      #@group = Group.find(params[:group_id])
    end
end
