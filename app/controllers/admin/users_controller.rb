class Admin::UsersController < Admin::AdminController
	load_and_authorize_resource
	before_action :set_user, only: [:show, :edit, :update, :destroy]
	def index
		@users = User.all.order(:name).page params[:page]
	end

	def new
		add_breadcrumb "Usuários", :admin_users_path
		add_breadcrumb "Criar Usuário"

		@user = User.new
	end

	def create
		@user = User.new(params[:user])
    	respond_to do |format|
			if @user.save
				format.html { redirect_to admin_users_path, notice: 'Usuário criado com sucesso' }
			else
				format.html { render :new }
			end
		end
	end

	def edit
		add_breadcrumb "Usuários", :admin_users_path
		add_breadcrumb "Editar Usuário"
	end

	def update
		@user = User.find(params[:id])
    	params[:user].delete(:password) if params[:user][:password].blank?
    	params[:user].delete(:password_confirmation) if params[:user][:password].blank? and params[:user][:password_confirmation].blank?
    	
    	if @user.update(user_params)
	      redirect_to admin_users_path, notice: 'Usuário atualizado com sucesso'
	    else
	      render :action => 'edit'
	    end
	end

  def show

  end

  def destroy
    @user.destroy
    redirect_to admin_users_path
  end

	private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:email, :name, :role, :password, :password_confirmation)
    end

end
