class Admin::AlternativesListsController < Admin::AdminController
	authorize_resource :class => Admin::AlternativesListsController

	before_action :set_alternative, only: [:show, :edit, :update, :destroy]
  	before_action :set_event_research_group_question_father_question, only: [:show, :edit, :new, :index, :create, :destroy, :update]

  	def index
  		@alternatives = Alternative.where(question_id: @question).order(:id)
  	end

  	def show
  	end

  	def edit
  	end

  	def new
  		@alternative = Alternative.new
  	end

  	def create  		
  		@alternative = Alternative.new(alternative_params)
  		@alternative.question_id = @question.id
  		@alternative.as_other ||= 0	
	    respond_to do |format|
	      if @alternative.save
	        format.html { redirect_to admin_event_research_group_question_question_list_alternative_path(@event, @research, @group, @question_father, @question, @alternative), notice: 'Alternative was successfully created.' }
	        format.json { render :show, status: :created, location: @alternative }
	      else
	        format.html { render :new }
	        format.json { render json: @alternative.errors, status: :unprocessable_entity }
	      end
	    end
  	end

  	def update
  		respond_to do |format|
	      if @alternative.update(alternative_params)
	        format.html { redirect_to admin_event_research_group_question_question_list_alternative_path(@event, @research, @group, @question_father, @question, @alternative), notice: 'Alternative was successfully updated.' }
	        format.json { render :show, status: :ok, location: @alternative }
	      else
	        format.html { render :edit }
	        format.json { render json: @alternative.errors, status: :unprocessable_entity }
	      end
	    end
  	end

  	def destroy
  		@alternative.destroy
	    respond_to do |format|
	      format.html { redirect_to admin_event_research_group_question_question_list_alternatives_path, notice: 'Alternative was successfully destroyed.' }
	      format.json { head :no_content }
	    end
  	end

	private
		def set_alternative
			@alternative = Alternative.find(params[:id])
		end

		def alternative_params
			params.require(:alternative).permit(:title, :as_other, :question_id)
		end
		def set_event_research_group_question_father_question
			@event = Event.where(status: true).find(params[:event_id])
      		@research = Research.where(status: true).find(params[:research_id])
      		@group = Group.find(params[:group_id])
      		@question_father = Question.find(params[:question_id])
      		@question = Question.find(params[:question_list_id])
		end
end
