class Admin::AlternativesController < Admin::AdminController
  load_and_authorize_resource
  
  before_action :set_alternative, only: [:show, :edit, :update, :destroy]
  before_action :set_event_researche_group_question, only: [:show, :edit, :new, :index, :create, :destroy, :update]

  # GET /alternatives
  # GET /alternatives.json
  def index
    @alternatives = Alternative.where(question_id: @question).order(:id)
  end

  # GET /alternatives/1
  # GET /alternatives/1.json
  def show
  end

  # GET /alternatives/new
  def new
    @alternative = Alternative.new
  end

  # GET /alternatives/1/edit
  def edit
  end

  # POST /alternatives
  # POST /alternatives.json
  def create
    @alternative = Alternative.new(alternative_params)
    respond_to do |format|
      if @alternative.save
        format.html { redirect_to admin_event_research_group_question_alternative_path(@event, @research, @group, @question, @alternative), notice: 'Alternative was successfully created.' }
        format.json { render :show, status: :created, location: @alternative }
      else
        format.html { render :new }
        format.json { render json: @alternative.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /alternatives/1
  # PATCH/PUT /alternatives/1.json
  def update
    respond_to do |format|
      if @alternative.update(alternative_params)
        format.html { redirect_to admin_event_research_group_question_alternative_path(@event, @research, @group, @question, @alternative), notice: 'Alternative was successfully updated.' }
        format.json { render :show, status: :ok, location: @alternative }
      else
        format.html { render :edit }
        format.json { render json: @alternative.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /alternatives/1
  # DELETE /alternatives/1.json
  def destroy
    @alternative.destroy
    respond_to do |format|
      format.html { redirect_to admin_event_research_group_question_alternatives_path, notice: 'Alternative was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_alternative
      @alternative = Alternative.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def alternative_params
      params.require(:alternative).permit(:title, :as_other, :question_id)
    end

    def set_event_researche_group_question
      @event = Event.where(status: true).find(params[:event_id])
      @research = Research.where(status: true).find(params[:research_id])
      @group = Group.find(params[:group_id])

      if params["question_list_id"]
        @question_father = Question.find(params[:question_id])
        @question = Question.find(params[:question_list_id])
      else
        @question = Question.find(params[:question_id])
      end
    end
end
