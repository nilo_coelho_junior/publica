class Admin::AdminController < ApplicationController

	check_authorization

	before_filter :authenticate_user!

	rescue_from CanCan::AccessDenied do |exception|
    p "-----------------------------"
    p "Access denied on #{exception.action} #{exception.subject.inspect}"
    p "-----------------------------"
		flash[:error] = "Acesso não autorizado" 
		redirect_to new_user_session_path
	end
end
