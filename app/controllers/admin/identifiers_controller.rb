class Admin::IdentifiersController < Admin::AdminController
  load_and_authorize_resource

  before_action :set_identifier, only: [:show, :edit, :update, :destroy]
  before_action :set_event_research, only: [:show, :edit, :new, :index, :create, :destroy, :update]
  before_action :get_identifiers, only: [:index, :create, :update]

  # GET /identifiers
  # GET /identifiers.json
  def index
    add_breadcrumb "Eventos", :admin_events_path
    add_breadcrumb "#{@event.title}", :admin_events_path
    add_breadcrumb "#{@research.title}", :admin_event_researches_path
    add_breadcrumb "Identificadores", :admin_event_research_identifiers_path

  end

  # GET /identifiers/1
  # GET /identifiers/1.json
  def show
  end

  # GET /identifiers/new
  def new
    add_breadcrumb "Eventos", :admin_events_path
    add_breadcrumb "#{@event.title}", :admin_events_path
    add_breadcrumb "#{@research.title}", :admin_event_research_identifiers_path
    add_breadcrumb "Identificadores", :admin_event_research_identifiers_path
    add_breadcrumb "Criar Identificador"

    @identifier = Identifier.new
  end

  # GET /identifiers/1/edit
  def edit
    add_breadcrumb "Eventos", :admin_events_path
    add_breadcrumb "#{@event.title}", :admin_events_path
    add_breadcrumb "#{@research.title}", :admin_event_research_identifiers_path
    add_breadcrumb "#{@identifier.name}", :admin_event_research_identifiers_path
    add_breadcrumb "Editar", :edit_admin_event_research_identifier_path
  end

  # POST /identifiers
  # POST /identifiers.json
  def create
    identifier_params[:name].upcase!
    @identifier = Identifier.new(identifier_params)
    p "----------#{@identifier.valid?}"

    respond_to do |format|
      if @identifier.save
        format.html { redirect_to admin_event_research_identifiers_path(@event, @research, page: @identifiers.num_pages), notice: 'Identificador criado com sucesso' }
        format.json { render :show, status: :created, location: @identifier }
      else
        format.html { render :new }
        format.json { render json: @identifier.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /identifiers/1
  # PATCH/PUT /identifiers/1.json
  def update
    identifier_params[:name].upcase!
    respond_to do |format|
      if @identifier.update(identifier_params)
        format.html { redirect_to admin_event_research_identifiers_path(@event, @research, page: get_page(@identifier, @identifiers)), notice: 'Identificador atualizado com sucesso' }
        format.json { render :show, status: :ok, location: @identifier }
      else
        format.html { render :edit }
        format.json { render json: @identifier.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /identifiers/1
  # DELETE /identifiers/1.json
  def destroy
    @identifier.destroy
    respond_to do |format|
      format.html { redirect_to admin_event_research_identifiers_path(@event, @research), notice: 'Identificador removido com sucesso' }
      format.json { head :no_content }
    end
  end

  def copy
    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_identifier
      @identifier = Identifier.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def identifier_params
      params.require(:identifier).permit(:name, :research_ids)
    end

    def set_event_research
      @event = Event.find(params[:event_id])
      @research = Research.find(params[:research_id])
    end

    def get_identifiers
      if params[:search]
        @identifiers = @research.identifiers.where("LOWER(name) like LOWER(?)", "%#{params[:search]}%").order('id').page params[:page]
      else
        @identifiers = @research.identifiers.page params[:page]
      end
    end
end
