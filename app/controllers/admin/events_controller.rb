class Admin::EventsController < Admin::AdminController
  load_and_authorize_resource
  
  before_action :set_event, only: [:show, :edit, :update, :destroy, :report]
  before_action :get_events, only: [:index, :create, :update]

  # GET /events
  # GET /events.json
  def index
    
  end

  # GET /events/1
  # GET /events/1.json
  def show
  end

  # GET /events/new
  def new
    add_breadcrumb "Eventos", :admin_events_path
    add_breadcrumb "Criar Evento"

    @event = Event.new
  end

  # GET /events/1/edit
  def edit
    add_breadcrumb "Eventos", :admin_events_path
    add_breadcrumb "Editar #{@event.title}", :edit_admin_event_path
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)
    @event.author = current_user
    
    respond_to do |format|
      if @event.save
        format.html { redirect_to admin_events_path(page: @events.num_pages), notice: 'Evento criado com sucesso' }
        format.json { render :show, status: :created, location: new_admin_event_path(@event) }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    
    @event.status.to_i

    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to admin_events_path(page: get_page(@event, @events)), notice: 'Evento atualizado com sucesso' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.desativado!
    respond_to do |format|
      format.html { redirect_to admin_events_path, notice: 'Evento removido com sucesso' }
      format.json { head :no_content }
    end
  end

  def report
  end

  def search
    if !params[:search].nil? && !params[:search].blank?
      @search = Event.search(params[:search])
    end
    respond_to do |format|
      format.json {render json: @search}
    end
  end

  def sort
    respond_to do |format|
      format.json {render json: @events.order(params[:order])}
    end
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      params[:id] == nil ? @event = Event.find(params[:event_id]) : @event = Event.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:title, :description, :status, :author_id)
    end

    def get_events
      if params[:sort] == nil 
        params[:sort] = ' created_at desc'
      end


      if current_user.role == "pesquisador"
        @events = Event.tasker_with_surveys(current_user, params[:search]).order(params[:sort]).page params[:page]
      elsif current_user.role == "coordenador"
        @events = Event.for_coordinator(current_user, params[:search]).order(params[:sort]).page params[:page]
      elsif params[:search]
        @events = Event.search(params[:search]).order(params[:sort]).page params[:page]
      else
        @events = Event.active.order(params[:sort]).page params[:page]
      end
    end
end
