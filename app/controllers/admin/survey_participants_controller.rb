class Admin::SurveyParticipantsController < Admin::AdminController
	authorize_resource :class => Admin::SurveyParticipantsController

	before_action :set_survey_participant, only: [:show, :destroy, :edit]
	before_action :set_event_research, only: [:show, :edit, :new, :index, :create, :destroy, :update]
	before_action :set_users, only: [:new, :edit]

	def index
		add_breadcrumb "Eventos", :admin_events_path
    add_breadcrumb "#{@event.title}", :admin_events_path
    add_breadcrumb "#{@research.title}", :admin_event_researches_path
    add_breadcrumb "Questionários", :admin_event_research_surveys_path
    add_breadcrumb "Pesquisadores"

		if params[:search] and !params[:search].empty?
			user = User.find_by("LOWER(name) LIKE LOWER(?)", "#{params[:search]}%")
			@participants = @research.surveys.joins(:participant).select("count(participant_id) as total_survey, participant_id, name").where("surveys.participant_id = ?", user).group('participant_id, name').page params[:page]
		else
			@participants = @research.surveys.joins(:participant).select("count(participant_id) as total_survey, participant_id, name").group('participant_id, name').page params[:page]
		end
    
	end

	def new
		add_breadcrumb "Eventos", :admin_events_path
    add_breadcrumb "#{@event.title}", :admin_events_path
    add_breadcrumb "#{@research.title}", :admin_event_researches_path
    add_breadcrumb "Questionários", :admin_event_research_surveys_path
    add_breadcrumb "Pesquisadores", :admin_event_research_survey_participants_path
    add_breadcrumb "Inserir Pesquisador"
	end

	def create
		participant = User.find(params["survey_participants"]["participant"])
		surveys_start = params["survey_participants"]["surveys_start"]
		surveys_end = params["survey_participants"]["surveys_end"]

		@surveys.where(number_survey: surveys_start..surveys_end).each do |survey|
			survey.update(participant: participant)
		end

		respond_to do |format|
			format.html { redirect_to admin_event_research_survey_participants_path, notice: 'Participante inserido com sucesso' }
		  format.json { render :show, status: :created, location: admin_event_research_survey_participants_path }
		end
	end

	def edit
		add_breadcrumb "Eventos", :admin_events_path
    add_breadcrumb "#{@event.title}", :admin_events_path
    add_breadcrumb "#{@research.title}", :admin_event_researches_path
    add_breadcrumb "Questionários", :admin_event_research_surveys_path
    add_breadcrumb "Pesquisadores", :admin_event_research_survey_participants_path
    add_breadcrumb "#{@participant.name}", :admin_event_research_survey_participants_path
    add_breadcrumb "Editar"
	end

	def update
		participant = User.find(params["survey_participants"]["participant"])
		surveys_start = params["survey_participants"]["surveys_start"]
		surveys_end = params["survey_participants"]["surveys_end"]

		if @surveys.first.number_survey.to_i == 0
			@surveys = @surveys.where(number_survey: surveys_start..surveys_end)
		else
			@surveys = @surveys.where("(surveys.number_survey)::INT between ? and ?", surveys_start.to_i, surveys_end.to_i)
		end
		if @surveys.update_all(participant_id: participant)

			respond_to do |format|
				format.html { redirect_to admin_event_research_survey_participants_path, notice: 'Participante ataulizado com sucesso' }
			  format.json { render :show, status: :created, location: admin_event_research_survey_participants_path }
			end

		end

	end

	def show
	end

	def destroy
		@survey.each do |survey|
			survey.update(participant_id: nil)
		end

		respond_to do |format|
			format.html { redirect_to admin_event_research_survey_participants_path(@event, @research), notice: 'Participante removido com sucesso' }
		    format.json { render :show, status: :created, location: admin_event_research_survey_participants_path(@event, @research) }
		end

	end

	private	

		def set_users
			if params[:action] == 'edit'
				@users = User.survey_taskers
			else
				@users = User.whithout_participant(@research)
			end
		end

		def set_survey_participant
			@participant = User.find(params[:id])
		end

		def survey_participants_params
	      params.require(:survey_participants).permit(:participant, :survey_start, :survey_end)
	    end

		def set_event_research
      @event = Event.where(status: true).find(params[:event_id])
  	  @research = Research.where(status: true).find(params[:research_id])

  	  if ['edit', 'update'].include? params[:action]
  	  	@surveys = @research.surveys.where("participant_id is null or participant_id = ?", params[:id])
  	  else
  	  	@surveys = @research.surveys.where("participant_id is null")

        if !/\W/.match(@surveys.first.number_survey)
          @surveys = @surveys.order("(number_survey)::INT").page params[:page]
        else
          @surveys = @surveys.order("number_survey").page params[:page]
        end
  	  end
  	  
    end
end
