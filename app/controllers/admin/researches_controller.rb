class Admin::ResearchesController < Admin::AdminController
  load_and_authorize_resource
  
  before_action :set_event
  before_action :set_research, only: [:show, :edit, :update, :destroy]
  before_action :set_survey, only: [:report]
  before_action :get_researches, only: [:index, :create, :update]

  # GET /researches
  # GET /researches.json
  def index
    add_breadcrumb "Eventos", :admin_events_path
    add_breadcrumb "#{@event.title}", :admin_events_path
    add_breadcrumb "Pesquisas", :admin_event_researches_path
  end

  # GET /researches/1
  # GET /researches/1.json
  def show
  
    # @report = render_to_string(action: "report", locals: {colors: @colors, number: @number})
  end

  # GET /researches/new
  def new
    add_breadcrumb "Eventos", :admin_events_path
    add_breadcrumb "#{@event.title}", :admin_events_path
    add_breadcrumb "Pesquisas", :admin_event_researches_path
    add_breadcrumb "Criar Pesquisas"

    @research = Research.new
    @researches_identifiers = Research.with_identifiers(@event.id)
  end

  # GET /researches/1/edit
  def edit
    add_breadcrumb "Eventos", :admin_events_path
    add_breadcrumb "#{@event.title}", :admin_events_path
    add_breadcrumb "#{@research.title}", :admin_event_researches_path
    add_breadcrumb "Editar", :edit_admin_event_research_path
  end

  # POST /researches
  # POST /researches.json
  def create
    @research = Research.new(research_params)
    @research.event_id = params[:event_id]
    @research.author = current_user

    if @research.save
      if !params["research_identifiers"]["research_id"].blank?
        @research_original = Research.find(params["research_identifiers"]["research_id"])
        @research.identifiers = @research_original.identifiers.clone
        
        @research_original.surveys.each do |survey_original|
          survey = Survey.new(research_id: @research.id, number_survey: survey_original.number_survey, author_id: current_user.id)
          if survey.save
            survey_identifier = SurveyIdentifier.new
            survey_identifier.survey_id = survey.id
            survey_identifier.identifiers = survey_original.survey_identifier.identifiers
            survey_identifier.save
          end
        end
      end
    end

    respond_to do |format|
      if @research.persisted?
        format.html { redirect_to admin_event_researches_path(@event, page: @researches.num_pages), notice: 'Pesquisa criada com sucesso' }
        format.json { render :show, status: :created, location: admin_event_research_path(@event, @research) }
      else
        format.html { render :new }
        format.json { render json: @research.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /researches/1
  # PATCH/PUT /researches/1.json
  def update
    respond_to do |format|
      if @research.update(research_params)
        format.html { redirect_to admin_event_researches_path(@event, page: get_page(@research, @researches)), notice: 'Pesquisa atualziada com sucesso' }
        format.json { render :show, status: :ok, location: admin_event_research_path(@event, @research) }
      else
        format.html { render :edit }
        format.json { render json: @research.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /researches/1
  # DELETE /researches/1.json
  def destroy
    @research.destroy
    respond_to do |format|
      format.html { redirect_to admin_event_researches_path(@event), notice: 'Pesquisa removida com sucesso' }
      format.json { head :no_content }
    end
  end

  def report
    add_breadcrumb "Eventos", :admin_events_path
    add_breadcrumb "#{@event.title}", :admin_events_path
    add_breadcrumb "#{@research.title}", :admin_event_researches_path
    add_breadcrumb "Relatório"

    @colors = 10.times.map{"%06x" % (rand * 0x1000000)}
    @number = 10000.times.map{Random.rand(1...10000)}
    @research.identifiers.preload(:survey_identifiers)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_research
      @research = @event.researches.find(params[:id])
    end

    def set_event
      @event = Event.where(status: true).find(params[:event_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def research_params
      params.require(:research).permit(:title, :description, :date_start, :date_end, :status, :author, research_identifiers: [:research_id], user_ids: [])
    end

    def set_survey
      @research = @event.researches.find(params[:research_id])
      #@research = Research.includes(:groups => [:questions => :alternatives]).find(params[:research_id])
      #@surveys = Survey.includes(:answers).where(research_id: params[:research_id])
    end

    def get_researches
      if params[:sort] == nil 
        params[:sort] = ' created_at desc'
      end
      
      if current_user.role == "Pesquisador"
        @researches = Research.tasker_with_surveys(params[:event_id], current_user, params[:search]).order(params[:sort]).page params[:page]
      elsif current_user.role == "Coordenador"
        @researches = Research.for_coordinator(params[:event_id], current_user, params[:search]).order(params[:sort]).page params[:page]
      else
        @researches = Research.search(params[:event_id], params[:search]).order(params[:sort]).page params[:page]
      end
    end
end
