require 'json'

class Admin::AnswersController < Admin::AdminController
  load_and_authorize_resource
  
  before_action :set_event_research, only: [:show, :edit, :new, :index, :create, :destroy, :update]
  before_action :set_answer, only: [:show, :edit, :update, :destroy]
  # GET /answers
  # GET /answers.json
  def index
    add_breadcrumb "Eventos", :admin_events_path
    add_breadcrumb "#{@event.title}", :admin_events_path
    add_breadcrumb "#{@research.title}", :admin_event_researches_path
    add_breadcrumb "Questionários", :admin_event_research_surveys_path
    add_breadcrumb @survey.number_survey
    add_breadcrumb "Resposta"

  end

  # GET /answers/1
  # GET /answers/1.json
  def show
  end

  # GET /answers/new
  def new
    p "---------------------------------------------"
    p @survey
    p "---------------------------------------------"
    add_breadcrumb "Eventos", :admin_events_path
    add_breadcrumb "#{@event.title}", :admin_events_path
    add_breadcrumb "#{@research.title}", :admin_event_researches_path
    add_breadcrumb "Questionários", :admin_event_research_surveys_path
    add_breadcrumb @survey.number_survey
    add_breadcrumb "Cadastrar Resposta"
    
  end

  # GET /answers/1/edit
  def edit
    add_breadcrumb "Eventos", :admin_events_path
    add_breadcrumb "#{@event.title}", :admin_events_path
    add_breadcrumb "#{@research.title}", :admin_event_researches_path
    add_breadcrumb "Questionários", :admin_event_research_surveys_path
    add_breadcrumb @survey.number_survey
    add_breadcrumb "Editar Resposta"

    @answer = @survey.answers.first
  end
  # POST /answers
  # POST /answers.json
  def create

    @survey.answers.destroy_all

    params["question"].reject{|a| a == "undefined"}.each do |answer|
      
      question = Question.find(answer[0])
      
      # question text
      if question.type_question == "texto" and answer[1]["alternative"].kind_of? String
        p "question text"
        Answer.create(survey_id: @survey.id, question_id: answer[0], alternative_id: answer[1]["alternative"], value: answer[1]["value"])
      
      elsif answer[1]["alternative"].kind_of? Array
        p "question Array"
        answer[1]["alternative"].reject{|id| id.empty?}.each do |a|
          if Alternative.find(a).as_other == 0
            Answer.create(survey_id: @survey.id, question_id: answer[0], alternative_id: a)
          else
            Answer.create(survey_id: @survey.id, question_id: answer[0], alternative_id: a, value: answer[1]["value"])
          end  
        end
      elsif question.type_question == "unica_escolha" and answer[1]["alternative"].kind_of? String
        p "question unica_escolha"
        if Alternative.find(answer[1]["alternative"]).as_other == 0
          Answer.create(survey_id: @survey.id, question_id: answer[0], alternative_id: answer[1]["alternative"])
        else
          Answer.create(survey_id: @survey.id, question_id: answer[0], alternative_id: answer[1]["alternative"], value: answer[1]["value"])
        end
      else
        p answer[1]
        answer[1].each_with_index do |lista, index|
          p "------------------------------------------------------------------------"
          p lista
          p index
          if lista[1]["status"] != "remove"
            if question.type_question == "texto" and lista[1]["alternative"].kind_of? String
              Answer.create(survey_id: @survey.id, question_id: question.id, alternative_id: lista[1]["alternative"], value: lista[1]["value"], order: index)

            elsif lista[1]["alternative"].kind_of? Array
              lista[1]["alternative"].reject{|id| id.empty?}.each do |a|

                if Alternative.find(a).as_other == 0
                  Answer.create(survey_id: @survey.id, question_id: question.id, alternative_id: a, order: index)
                else
                  Answer.create(survey_id: @survey.id, question_id: question.id, alternative_id: a, value: lista[1]["value"], order: index)
                end  
              end
            elsif question.type_question == "unica_escolha" and lista[1]["alternative"].kind_of? String
              if Alternative.find(lista[1]["alternative"]).as_other == 0
                Answer.create(survey_id: @survey.id, question_id: question.id, alternative_id: lista[1]["alternative"], order: index)
              else
                Answer.create(survey_id: @survey.id, question_id: question.id, alternative_id: lista[1]["alternative"], value: lista[1]["value"], order: index)
              end
            end
          end
          p "------------------------------------------------------------------------" 
        end
      end
    end
    
    respond_to do |format|      
      @survey.update(author: current_user)
      format.html { redirect_to admin_event_research_survey_answers_path, notice: 'Resposta salva com sucesso' }
      format.json { render :show, status: :created, location: @answer }      
    end
  end

  # PATCH/PUT /answers/1
  # PATCH/PUT /answers/1.json
  def update
    @survey.answers.destroy_all

    params["question"].reject{|a| a == "undefined"}.each do |answer|
      
      question = Question.find(answer[0])
      
      # question text
      if question.type_question == "texto" and answer[1]["alternative"].kind_of? String
        p "question text"
        p answer
        p answer[0]
        p answer[1]["alternative"]
        Answer.create(survey_id: @survey.id, question_id: answer[0], alternative_id: answer[1]["alternative"], value: answer[1]["value"])
      
      elsif answer[1]["alternative"].kind_of? Array
        p "question Array"
        answer[1]["alternative"].reject{|id| id.empty?}.each do |a|
          if Alternative.find(a).as_other == 0
            Answer.create(survey_id: @survey.id, question_id: answer[0], alternative_id: a)
          else
            Answer.create(survey_id: @survey.id, question_id: answer[0], alternative_id: a, value: answer[1]["value"])
          end  
        end
      elsif question.type_question == "unica_escolha" and answer[1]["alternative"].kind_of? String
        p "question unica_escolha"
        if Alternative.find(answer[1]["alternative"]).as_other == 0
          Answer.create(survey_id: @survey.id, question_id: answer[0], alternative_id: answer[1]["alternative"])
        else
          Answer.create(survey_id: @survey.id, question_id: answer[0], alternative_id: answer[1]["alternative"], value: answer[1]["value"])
        end
      else
        p answer[1]
        answer[1].each_with_index do |lista, index|
          p "------------------------------------------------------------------------"
          p lista
          p index
          
          if lista[1]["status"] != "remove"
            if question.type_question == "texto" and lista[1]["alternative"].kind_of? String
              Answer.create(survey_id: @survey.id, question_id: question.id, alternative_id: lista[1]["alternative"], value: lista[1]["value"], order: index)

            elsif lista[1]["alternative"].kind_of? Array
              lista[1]["alternative"].reject{|id| id.empty?}.each do |a|

                if Alternative.find(a).as_other == 0
                  Answer.create(survey_id: @survey.id, question_id: question.id, alternative_id: a, order: index)
                else
                  Answer.create(survey_id: @survey.id, question_id: question.id, alternative_id: a, value: lista[1]["value"], order: index)
                end  
              end
            elsif question.type_question == "unica_escolha" and lista[1]["alternative"].kind_of? String
              if Alternative.find(lista[1]["alternative"]).as_other == 0
                Answer.create(survey_id: @survey.id, question_id: question.id, alternative_id: lista[1]["alternative"], order: index)
              else
                Answer.create(survey_id: @survey.id, question_id: question.id, alternative_id: lista[1]["alternative"], value: lista[1]["value"], order: index)
              end
            end
          end

          p "------------------------------------------------------------------------" 
        end
      end
    end
    @survey.update(author: current_user)
    respond_to do |format|
        format.html { redirect_to admin_event_research_survey_answers_path(@event, @research, @survey), notice: 'Questionário salvo com sucesso' }
        format.json { render :show, status: :ok, location: @answer }
    end
  end

  # DELETE /answers/1
  # DELETE /answers/1.json
  def destroy
    @answer.destroy
    respond_to do |format|
      format.html { redirect_to research_answers_path, notice: 'Answer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_answer
      @answers = @survey.answers.first
    end

    def answer_params
      #params.require(:answer).permit(question: [ :value, :alternative, alternative: [] ])
    end

    def set_event_research
      @event = Event.where(status: 1).find(params[:event_id])
      @research = Research.where(status: 1).find(params[:research_id])
      @survey = @research.surveys.find_by(number_survey: params[:survey_number_survey])
    end
end
