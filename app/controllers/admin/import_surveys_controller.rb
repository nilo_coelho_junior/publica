class Admin::ImportSurveysController < Admin::AdminController
	authorize_resource :class => Admin::ImportSurveysController
	before_action :set_event_research, only: [:index, :import]

	def index
		add_breadcrumb "Eventos", :admin_events_path
    add_breadcrumb "#{@event.title}", :admin_events_path
    add_breadcrumb "#{@research.title}", :admin_event_researches_path
    add_breadcrumb "Questionários", :admin_event_research_surveys_path
    add_breadcrumb "Importar"
	end

	def import		

		file = CSV.read(params[:file].path, col_sep: ";", headers: true)

		@errors = []
		identifiers = []
		file.headers.each do |header|
			identifier = @research.identifiers.where(name: header.upcase)
			if  identifier.blank?
				@errors << header
			else
				identifier.each do |id|
					identifiers << id.id
				end
			end
		end

		if @errors.empty?
			file.each do |row|
				custom_identifiers = {}
				custom_values = row.to_h.values
				
				identifiers.each_with_index do |id, index|
					custom_identifiers.store(id.to_s, custom_values[index])
				end

				number_survey = row.to_h.values.join('-')
				survey = Survey.new(research: @research, number_survey: number_survey, author_id: current_user.id)
				if survey.save

					custom_identifiers.each do |item, value|
						survey_identifier = SurveyIdentifier.new(survey_id: survey.id, identifier_id: item, value: value)
						survey_identifier.save
					end
				else
					break
					respond_to do |format|
						format.html { render :import }
	        	format.json { render json: survey.errors, status: :unprocessable_entity }			
					end					
				end
			end
		end

		respond_to do |format|
	    if @errors.empty?
	      format.html { redirect_to admin_event_research_surveys_path(@event, @research), notice: 'Identifier was successfully created.' }
	      format.json { render :show, status: :created, location: @identifier }
	    else
	      format.html { render :index }
	      format.json { render json: @errors, status: :unprocessable_entity }
	    end
	   end
		  
	end

	private
		def set_event_research
	      @event = Event.where(status: true).find(params[:event_id])
      	  @research = Research.where(status: true).find(params[:research_id])
	    end
end
