class Admin::GroupsController < Admin::AdminController
  load_and_authorize_resource
  
  before_action :set_group, only: [:show, :edit, :update, :destroy]
  before_action :set_event_research, only: [:show, :edit, :new, :index, :create, :destroy, :update]

  # GET /groups
  # GET /groups.json
  def index
    add_breadcrumb "Eventos", :admin_events_path
    add_breadcrumb "#{@event.title}", :admin_events_path
    add_breadcrumb "#{@research.title}", :admin_event_researches_path
    add_breadcrumb "Perguntas"
    
    if @research.groups.empty?
      @research.groups.build.questions.build.alternatives.build
    end
  end

  # GET /groups/1
  # GET /groups/1.json
  def show
  end

  # GET /groups/new
  def new
    @group = Group.new
  end

  # GET /groups/1/edit
  def edit
  end

  # POST /groups
  # POST /groups.json
  def create
    p "create"
    asdsad
    @group = Group.new(group_params)
    @group.title.blank? ? @group.title = "Geral" : @group.title
    
    respond_to do |format|
      if @group.save
        format.html { redirect_to admin_event_research_group_path(@event, @research, @group), notice: 'Group was successfully created.' }
        format.json { render :show, status: :created, location: @group }
      else
        format.html { render :new }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /groups/1
  # PATCH/PUT /groups/1.json
  def update
    
    params["groups"].keys.each do |index|
      # grupo para ser removido?
      if params["groups"][index]["id"] and params["groups"][index]["status"] == "remove"
        p "remove grupo #{params["groups"][index]["title"]}"
        @research.groups.find(params["groups"][index]["id"].to_i).destroy
      elsif !params["groups"][index]["id"] and params["groups"][index]["status"] == "remove"
      else
        # atualiza grupo
        if params["groups"][index]["id"]
          p "atualiza grupo #{params["groups"][index]["title"]}"
          g = @research.groups.find(params["groups"][index]["id"].to_i)
          g.update(title: params["groups"][index]["title"])        
        # cria um novo grupo
        else
          p "cria grupo #{params["groups"][index]["title"]}"
          g = @research.groups.create(title: params["groups"][index]["title"], research_id: params[:id])
          asd
        end        

        params["groups"][index]["question"].keys.each do |question|

          if params["groups"][index]["question"][question]["id"] and params["groups"][index]["question"][question]["status"] == "remove"
            p "remove question #{params["groups"][index]["question"][question]["title"]}"
            q = g.questions.find(params["groups"][index]["question"][question]["id"].to_i)
            q.destroy
            
          elsif !params["groups"][index]["question"][question]["id"] and params["groups"][index]["question"][question]["status"] == "remove"

          else
            if params["groups"][index]["question"][question]["id"] and params["groups"][index]["question"][question]["status"] == "update_type_question"
              p "update question #{params["groups"][index]["question"][question]["title"]}"
              q = g.questions.find(params["groups"][index]["question"][question]["id"].to_i)

              if q.type_question == "lista"
                p "question list"
                q.question_lists.destroy_all
              else
                p "question not list"
                q.alternatives.destroy_all
              end
              
              q.update(title: params["groups"][index]["question"][question]["title"], type_question: params["groups"][index]["question"][question]["type_question"])  

            elsif params["groups"][index]["question"][question]["id"]
              p "find question and update #{params["groups"][index]["question"][question]["title"]}"
              q = g.questions.find(params["groups"][index]["question"][question]["id"].to_i)
              q.update(title: params["groups"][index]["question"][question]["title"], type_question: params["groups"][index]["question"][question]["type_question"])

            else
              p "new question #{params["groups"][index]["question"][question]["title"]}"
              q = Question.create(title: params["groups"][index]["question"][question]["title"], type_question: params["groups"][index]["question"][question]["type_question"], group_id: g.id)
            end

            if q.type_question == "texto"
              p "question text, new alternative #{params["groups"][index]["question"][question]["title"]}"
              q.alternatives.create(title: params["groups"][index]["question"][question]["title"])
            elsif ["unica_escolha", "multipla_escolha"].include? q.type_question
              p "question unica_escolha,multipla_escolha"
              params["groups"][index]["question"][question]["alternative"].keys.each do |alternative|
                if params["groups"][index]["question"][question]["alternative"][alternative]["id"] and params["groups"][index]["question"][question]["alternative"][alternative]["status"] == "remove"
                  p "remove alternative #{params["groups"][index]["question"][question]["alternative"][alternative]["title"]}"
                  a = q.alternatives.find(params["groups"][index]["question"][question]["alternative"][alternative]["id"].to_i)
                  a.destroy
                elsif !params["groups"][index]["question"][question]["alternative"][alternative]["id"] and params["groups"][index]["question"][question]["alternative"][alternative]["status"] == "remove"

                elsif params["groups"][index]["question"][question]["alternative"][alternative]["id"]
                  p "update alternative #{params["groups"][index]["question"][question]["alternative"][alternative]["title"]}"
                  a = q.alternatives.find(params["groups"][index]["question"][question]["alternative"][alternative]["id"].to_i)
                  a.update(title: params["groups"][index]["question"][question]["alternative"][alternative]["title"], as_other: params["groups"][index]["question"][question]["alternative"][alternative]["as_other"])
                else
                  p "new alternative #{params["groups"][index]["question"][question]["alternative"][alternative]["title"]}"
                  q.alternatives.create(title: params["groups"][index]["question"][question]["alternative"][alternative]["title"], as_other: params["groups"][index]["question"][question]["alternative"][alternative]["as_other"])
                end
              end
            elsif q.type_question == "lista"
              p "question lista"
              params["groups"][index]["question"][question]["lista"].keys.each do |lista|
                if params["groups"][index]["question"][question]["lista"][lista]["id"] and params["groups"][index]["question"][question]["lista"][lista]["status"] == "remove"
                  p "question list remove #{params["groups"][index]["question"][question]["lista"][lista]["title"]}"
                  ql = Question.find(params["groups"][index]["question"][question]["lista"][lista]["id"].to_i)
                  ql.destroy
                elsif !params["groups"][index]["question"][question]["lista"][lista]["id"] and params["groups"][index]["question"][question]["lista"][lista]["status"] == "remove"
                  
                else
                  if params["groups"][index]["question"][question]["lista"][lista]["id"] and params["groups"][index]["question"][question]["lista"][lista]["status"] == "update_type_question"
                    p "question list update #{params["groups"][index]["question"][question]["lista"][lista]["title"]}"
                    ql = Question.find(params["groups"][index]["question"][question]["lista"][lista]["id"].to_i)
                    ql.alternatives.destroy_all                  
                    ql.update(title: params["groups"][index]["question"][question]["lista"][lista]["title"], type_question: params["groups"][index]["question"][question]["lista"][lista]["type_question"])

                  elsif params["groups"][index]["question"][question]["lista"][lista]["id"]
                    p "question list find and update #{params["groups"][index]["question"][question]["lista"][lista]["title"]}"
                    ql = Question.find(params["groups"][index]["question"][question]["lista"][lista]["id"].to_i)
                    ql.update(title: params["groups"][index]["question"][question]["lista"][lista]["title"], type_question: params["groups"][index]["question"][question]["lista"][lista]["type_question"])

                  else
                    p "question list new #{params["groups"][index]["question"][question]["lista"][lista]["title"]}"
                    ql = g.questions.create(title: params["groups"][index]["question"][question]["lista"][lista]["title"], type_question: params["groups"][index]["question"][question]["lista"][lista]["type_question"], father_id: q.id)
                  end

                  if ql.type_question == "texto"
                    p "question list = texto"
                    ql.alternatives.create(title: params["groups"][index]["question"][question]["lista"][lista]["title"])
                  else
                    p "question list unica_escolha, multipla_escolha"
                    params["groups"][index]["question"][question]["lista"][lista]["alternative"].keys.each do |alternative|

                      if params["groups"][index]["question"][question]["lista"][lista]["alternative"][alternative]["id"] and params["groups"][index]["question"][question]["lista"][lista]["alternative"][alternative]["status"] == "remove"
                        p "alternative remove #{params["groups"][index]["question"][question]["lista"][lista]["title"]}"
                        a = Alternative.find(params["groups"][index]["question"][question]["lista"][lista]["alternative"][alternative]["id"].to_i)
                        a.destroy

                      elsif !params["groups"][index]["question"][question]["lista"][lista]["alternative"][alternative]["id"] and params["groups"][index]["question"][question]["lista"][lista]["alternative"][alternative]["status"] == "remove"

                      elsif params["groups"][index]["question"][question]["lista"][lista]["alternative"][alternative]["id"]
                        p "alterantive update #{params["groups"][index]["question"][question]["lista"][lista]["title"]}"
                        a = Alternative.find(params["groups"][index]["question"][question]["lista"][lista]["alternative"][alternative]["id"].to_i)
                        a.update(title: params["groups"][index]["question"][question]["lista"][lista]["alternative"][alternative]["title"], as_other: params["groups"][index]["question"][question]["lista"][lista]["alternative"][alternative]["as_other"])
                      else
                        p "alternative new #{params["groups"][index]["question"][question]["lista"][lista]["title"]}"
                        ql.alternatives.create(title: params["groups"][index]["question"][question]["lista"][lista]["alternative"][alternative]["title"], as_other: params["groups"][index]["question"][question]["lista"][lista]["alternative"][alternative]["as_other"])
                      end
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
    
    respond_to do |format|
      format.html { redirect_to groups_admin_event_research_path(@event, @research), notice: 'Perguntas salvas com sucesso!' }
      format.json { render :show, status: :ok, location: @group }
    end
  end

  # DELETE /groups/1
  # DELETE /groups/1.json
  def destroy
    @group.destroy
    respond_to do |format|
      format.html { redirect_to admin_event_research_groups_path(@event, @research), notice: 'Group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_group
      @group = Group.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def group_params  
      #params.require(:groups).permit(group_attributes: [:id, :title], questions_attributes: [:id, :title, :type_question, alternatives_attributes: [:id, :title]])
    end

    def set_event_research
      @event = Event.where(status: true).find(params[:event_id])
      @research = Research.where(status: true).find(params[:id])
    end
end
