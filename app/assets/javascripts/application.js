 // This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require foundation/js/vendor/modernizr.js
//= require foundation/js/vendor/jquery.js
//@ sourceMappingURL=jquery/dist/jquery.min.map
//= require jquery-ujs/src/rails.js
//= require fastclick/lib/fastclick.js
//= require foundation/js/foundation.min.js
//= require sweetalert/lib/sweet-alert.min.js

$(document).foundation();

//Busca identificadores

function getIdentifier(research, identifier, elem){
	
	classe = $(elem).attr("class")
	value = ""

	if (elem.value == ""){
		$.each($("."+classe), function(index, object){
			if ($("."+classe).index(object) >= $("."+classe).index($(elem))){
				$("."+classe).eq(index).find("option").remove();
			}
		});
	}else{
		$.each($("."+classe), function(index, object){
			if ($("."+classe).index(object) <= $("."+classe).index($(elem))){
				if (index > 0){
					value = value.concat("-"+object.value)
				}else{
					value = value.concat(object.value)
				}
			}
		});	

		$.get("/ajax/identifiers?research="+research+"&id="+identifier+"&value="+value, function(data){
			
			if ($("."+classe).index($(elem)) <= $("."+classe).length-1){
				
				next = $("."+classe).eq($("."+classe).index($(elem)) + 1)
				next.find("option").remove();
				$.each(data, function(index, object){
					if (index == 0){
						next.append('<option value="">Selecionar Todos</option>')
					}
					next.append('<option value="'+object.value+'">'+object.value+'</option>')
				});
			}
		}, "json");
	}
}

//Report Event
function eventReport(){
	if ($(".event").length > 0){
		console.log("Has event");
		$.each($(".report"), function(index, r){
			$.ajax({
				type: "GET",
				url: "/ajax/report/",
				dataType: "json",
				async: true,
				data: {research: $(".report").eq(index).attr("research")},
				success: function(data){
					alternatives = []
					$.each(data, function(i, object){
						if (i == 0){
							actual_id = object.question_id	
							question_id = $(".report").eq(index).find("#question_id_"+object.question_id)
						}

						if (actual_id == object.question_id){
							alternatives[alternatives.length] = object
						}else{
							rows = []
							
							if (alternatives[0].question_type == 1){
								
								id = Math.floor((Math.random() * 1000) + 1);
								$(question_id).append('<div id="pngchart'+id+'"></div>');
								$(question_id).append('<div id="chart'+id+'"></div>');
								
								$.each(alternatives, function(index, alternative){
									rows[rows.length] = [alternative.title+"("+alternative.total+")", alternative.total]
								});
								
						  	google.setOnLoadCallback(drawChart);

						  	function drawChart() {

									var data = new google.visualization.DataTable();
							    data.addColumn('string', 'annotation');
							    data.addColumn('number', 'Slices');
							    data.addRows(rows);

							    var options = {'width':1000, 'height':400};

							    window["chart"+id] = new google.visualization.PieChart(document.getElementById("chart"+id));

							    google.visualization.events.addListener(window["chart"+id], 'ready', function () {
							      	window["chart"+id].innerHTML = '<img src="' + window["chart"+id].getImageURI() + '">';
							      	document.getElementById('pngchart'+id).outerHTML = '<a href="' + window["chart"+id].getImageURI() + '" target="_blank" class=" button tiny radius" style="padding-top: 0.325rem;padding-right: 0.325rem;padding-bottom: 0.3875rem;padding-left: 0.325rem;">Gerar imagem</a>';
								  });
								  window["chart"+id].draw(data, options);
							  }
							  drawChart();
							}else{
								
								id = Math.floor((Math.random() * 1000) + 1);
								$(question_id).append('<div id="pngchart'+id+'"></div>');
								$(question_id).append('<div id="chart'+id+'"></div>');

								rows[0] = ['Pergunta', 'Total', { role: 'style' }, { role: 'annotation' }]
								$.each(alternatives, function(index, alternative){
									rows[rows.length] = [alternative.title, alternative.total, '#'+Math.floor(Math.random()*16777215).toString(16), alternative.total]
								});
								
								google.setOnLoadCallback(barChart);

						  	function barChart(){

						  		var data = google.visualization.arrayToDataTable(rows);

					        var options = {
					          hAxis: {title: 'Pergunta', titleTextStyle: {color: 'blue'}},
					          vAxis: {title: 'Total de Respostas', titleTextStyle: {color: 'blue'}},
					          chartArea: {width: '50%'},
					          legend: {position: 'rigth'}
					        };

					        var view = new google.visualization.DataView(data);
						      view.setColumns([0, 1,
						                       { calc: "stringify",
						                         sourceColumn: 1,
						                         type: "string",
						                         role: "annotation" },
						                       2]);
					        window["chart"+id] = new google.visualization.BarChart(document.getElementById('chart'+id));

					        google.visualization.events.addListener( window["chart"+id], 'ready', function () {
						      	 window["chart"+id].innerHTML = '<img src="' +  window["chart"+id].getImageURI() + '">';
						      	document.getElementById('pngchart'+id).outerHTML = '<a href="' +  window["chart"+id].getImageURI() + '" target="_blank" class=" button tiny radius" style="padding-top: 0.325rem;padding-right: 0.325rem;padding-bottom: 0.3875rem;padding-left: 0.325rem;">Gerar imagem</a>';
						    	});
					        window["chart"+id].draw(view, data, options);
						  	}
						  	barChart();
							}
							question_id = $(".report").eq(index).find("#question_id_"+object.question_id)
							actual_id = object.question_id	
							alternatives = []
							alternatives[alternatives.length] = object
						}
					});
					$(".loading").addClass("hide")
					$(".report").eq(index).removeClass("hide")
				}
			});

		});

	}
}

//url para json search
function seturl(type){
	return window.location.pathname + "/"+type;
}

//url de redirecionamento
function redirectUrl(type, id){
	if(type == "event"){
		return window.location.pathname + "/" + id + "/researches"; 
	}
}

$(document).ready(function(){
	$(".alert-box").fadeIn(3000);
	$(".alert-box").fadeOut(3000);

	//Impede que o usuário selecione uma alternativa quanto ele preenche a resposta como outro(a)
	$(".alternatives input[type=text]").change(function(){
		
		if ($(this).val().length > 0) {
			$(this).parent().find(".as_other").prop("checked", true);
		}else{
			$(this).parent().find(".as_other").prop("checked", false);
		}
	});

	$(".alternatives input[type=radio]", ".alternatives input[type=checkbox]").click(function(){
		if ($(this).parent().parent().parent().add(".as_other").not(":checked")){
			$(this).parent().parent().parent().find("input[type=text]").val("");
		}
	});

	$(".alternatives input[type=checkbox]").click(function(){
		if (!$(this).parent().parent().parent().find(".as_other").is(":checked")){
			$(this).parent().parent().parent().find("input[type=text]").val("");
		}
	});

	eventReport();

	//Report Research Original
	$("#report").submit(function(event){
		$.ajax({
			type: "GET",
			url: "/ajax/report/",
			dataType: "json",
			async: true,
			data: {
				research: $("#research").val(),
				data: $("#report").serializeArray()
			},
			beforeSend: function(){
				$(".message").addClass("hide");
				$(".loading").removeClass("hide");
				if (!$(".report").hasClass("hide")){
					$(".report").addClass("hide");	
				}
			},
			success: function(data){
				alternatives = []
				color = ["#1E824C", "#36D7B7", "#049372", "#2ECC71", "#2ABB9B", "#1BA39C", "#03C9A9", "#019875", "#1BA39C", "#03C9A9", "#26C281"]
				
				$.each(data, function(index, object){
					
					if (index == 0){
						actual_id = object.question_id	
						question_id = "#question_id_"+object.question_id
					}

					if (actual_id == object.question_id && index != (data.length-1)){
						alternatives[alternatives.length] = object
					}else{
						if (index == (data.length-1)){
							alternatives[alternatives.length] = object
						}
						rows = []
						
						$(question_id).find("div").remove();
						$(question_id).find("a").remove();

						if (alternatives[0].question_type == 1 || alternatives[0].question_type == 2){

							id = Math.floor((Math.random() * 1000) + 1);
							$(question_id).append('<div id="pngchart'+id+'"></div>');
							$(question_id).append('<div id="chart'+id+'" style="width=800px;"></div>');

							rows[0] = ['Pergunta', 'Total', { role: 'style' }, { role: 'annotation' }]
							
              max = 0;
              $.each(alternatives, function(index, alternative){
                max += alternative.total
              });

							$.each(alternatives, function(index, alternative){
                percent = Math.round((alternative.total/max)*100)
                
								rows[rows.length] = [alternative.title+'('+alternative.total+')', percent, color[index], ''+percent+'%']
							});
              
							google.setOnLoadCallback(barChart);

					  	function barChart(){

					  		var data = google.visualization.arrayToDataTable(rows);

				        var options = {
				        	width: $(".toolbox").width(), 
				          height: 400,
				          hAxis: {title: '', titleTextStyle: {color: 'transparent'}},
				          vAxis: {format: '#\'%\'', title: '', titleTextStyle: {color: 'transparent'}},
									chartArea: {left: '45%', width: '50%'},
									legend: {position: 'none'},
                  annotations: {
                    alwaysOutside: true
                  }
				        };

				        window["chart"+id] = new google.visualization.BarChart(document.getElementById('chart'+id));

				        google.visualization.events.addListener( window["chart"+id], 'ready', function () {
				        	window["chart"+id].innerHTML = '<img src="' +  window["chart"+id].getImageURI() + '">';
					      	document.getElementById('pngchart'+id).outerHTML = '<a href="' +  window["chart"+id].getImageURI() + '" target="_blank" class=" button tiny radius" style="padding-top: 0.325rem;padding-right: 0.325rem;padding-bottom: 0.3875rem;padding-left: 0.325rem;">Gerar Imagem</a>';
					    	});
				        window["chart"+id].draw(data, options);
					  	}
					  	google.setOnLoadCallback(barChart);
					  	barChart();
						}
						question_id = "#question_id_"+object.question_id
						actual_id = object.question_id	
						alternatives = []
						alternatives[alternatives.length] = object
					}
				});

				$(".loading").addClass("hide");
				$(".report").removeClass("hide");
				$.each($("fieldset div[id^='question_id']"), function(index){
					if ((index+1)%3 == 0){
						$(this).addClass("page-break");
					}
				});
			}
		});
		event.preventDefault();
	});
	
	
  $("#sortable, #searchable #de, #searchable #ate").on("change", function() {
    $.get($("#searchable").attr("action"), $("#searchable").serialize(), null, "script");
    return false;
  });

  $("#searchable input").keyup(function() {
    $.get($("#searchable").attr("action"), $("#searchable").serialize(), null, "script");
    return false;
  });

  $("dd a").on("click", function(){
  	$(".meter").css("width", ($("dd a").index($(this)) + 1) *100/$("dd a").length + "%");
  });

  $(".tabs-content a").on("click", function(){
  	console.log($("dd a"+$(this).attr("href")));
  	$("dd a"+$(this).attr("href")).click();
  });

  //Sweet Modal
  $(".sweet").on("click", function(event){
  	
  	url = $(this).attr("href");
  	id = $(this).data("id");

  	swal({
  		title: $(this).data("title"),
  		text: $(this).data("text"),
  		type: "warning",
  		showCancelButton: true,
  		allowOutsideClick: true,
  		confirmButtonColor: "#f62459",
  		confirmButtonText: "Excluir",
  		cancelButtonText: "Cancelar",
  		closeOnConfirm: true }, 
  		function(isConfirm){
  			if (isConfirm){
  				$.ajax({
  					url: url,
  					data: {id: id, "_method":"delete"},
  					dataType: "HTML",
  					type: "POST",
  					success: function(){
  						location.reload();
  					}
  				})
  			}
  		}
  	);
  	event.preventDefault();
  });

  //New Group
  $(document).on("click", ".add-group a", function(event){
  	console.log("New Group");
  	tabs = $("#groups .tabs");
  	abas = $("#groups .tabs dd").not(".tabs dd:hidden");
  	wrapper_content = $(".tabs-content");
  	new_tab = '<dd class="add-group"><a href="">+</a></dd>';
  	content = '';
  	group = abas.length-1;
  	abas.eq(abas.length-1).remove();
  	tabs.append('<dd class="active"><a href="#group'+abas.length+'">'+abas.length+'</a></dd>');
  	tabs.append(new_tab);

  	$(".content.active").removeClass("active");

  	group_content = '<div class="content large-12 columns active" id="group'+abas.length+'"><button class="icon-cancel group"></button><div class="title large-12 columns"> <div class="row"> <div class="large-1 columns"> <h6>Grupo</h6> </div> <div class="large-11 columns"> <input class="string required" data-group="'+group+'" id="research_groups_attributes_0_title" name="groups['+group+'][title]" placeholder="Nome do Grupo" type="text"></div> </div> </div> <div class="title large-12 columns"> <div class="row collpase"> <div class="large-1 columns"> <h6>Pergunta</h6> </div> <div class="large-11 columns"> <input class="string required question" data-group="'+group+'" data-question="0" id="research_groups_attributes_0_questions_attributes_0_title" name="groups['+group+'][question][0][title]" placeholder="Título da pergunta" type="text"> </div> </div> <div class="row collapse"> <div class="tipo large-11 columns right"> <div class="row collpase"> <div class="large-1 columns"> <h6>Tipo</h6> </div> <div class="large-11 columns"> <select class="select required type_question" data-group="'+group+'" data-question="0" id="research_groups_attributes_0_questions_attributes_0_type_question" name="groups['+group+'][question][0][type_question]"><option value="texto">Texto</option> <option value="unica_escolha">Unica Escolha</option> <option value="multipla_escolha">Multipla Escolha</option> <option value="lista">Lista</option></select> </div> </div> </div> </div> </div> <div class="row collpase"> <button class="nova-pergunta large-12 columns text-left"> Nova Pergunta </button> </div>';

  	wrapper_content.append(group_content);

  	$(".content.active input:first").focus();

  	event.preventDefault();
  });

	//Remove Group
	$(document).on("click", ".icon-cancel.group", function(event){
		console.log("Remove Group");
		tabs = $("#groups .tabs");
  	abas = $("#groups .tabs dd");
  	aba_active = $("dd.active");
  	group = parseInt($("dd.active a").text())-1;
  	aba_active.removeClass("active").hide();
  	abas.eq(aba_active.index()-1).addClass("active");
  	

  	content = $(".content");
  	content_active = $(".content.active");

  	content_active.append('<input class="hidden" name="groups['+group+'][status]" type="hidden" value="remove">')

  	content_active.removeClass("active").hide();
  	content.eq(content_active.index()-1).addClass("active");
  	event.preventDefault();
	});

  //Insert type_question
  $(document).on("change", ".type_question", function(event){

  	wrapper = $(this).parent().parent().parent().parent();
  	wrapper_tipo = $(this).parent().parent().parent();

  	wrapper_lista = wrapper.parent().parent().parent();

  	wrapper.find(".row.collapse").remove();
  	
  	if ($(this).val() == "unica_escolha" || $(this).val() == "multipla_escolha"){

  		if (wrapper_lista.hasClass("lista")){
  			count_lista = wrapper.parent().find("input:first").data("list");

  			wrapper.parent().find("input:first").after('<input type="hidden" name="groups['+$(this).data("group")+'][question]['+$(this).data("question")+'][lista]['+count_lista+'][status]" value="update_type_question">');

  			unica_escolha = '<div class="row collapse"> <div class="alternativa large-11 columns right"> <div class="alternativas"> <div> <button class="icon-cancel alternative"></button> <input class="string required" data-group="'+$(this).data("group")+'" data-question="'+$(this).data("question")+'" data-alternative="0" data-other="0" id="research_groups_attributes_'+$(this).data("group")+'_questions_attributes_'+$(this).data("question")+'_alternatives_attributes_0_title" name="groups['+$(this).data("group")+'][question]['+$(this).data("question")+'][lista]['+count_lista+'][alternative][0][title]" placeholder="Nova alternativa" type="text"> </div> </div> <div class="alternativa-opcao"> <button class="nova-alternativa" data-group="'+$(this).data("group")+'" data-question="'+$(this).data("question")+'">Nova alternativa</button>  ou <button class="outra-alternativa" data-group="'+$(this).data("group")+'" data-question="'+$(this).data("question")+'">adicionar Outra</button> </div> </div> </div>';
  		}else{
  			wrapper.parent().find("input:first").after('<input type="hidden" name="groups['+$(this).data("group")+'][question]['+$(this).data("question")+'][status]" value="update_type_question">');

  			unica_escolha = '<div class="row collapse"> <div class="alternativa large-11 columns right"> <div class="alternativas"> <div> <button class="icon-cancel alternative"></button> <input class="string required" data-group="'+$(this).data("group")+'" data-question="'+$(this).data("question")+'" data-alternative="0" data-other="0" id="research_groups_attributes_'+$(this).data("group")+'_questions_attributes_'+$(this).data("question")+'_alternatives_attributes_0_title" name="groups['+$(this).data("group")+'][question]['+$(this).data("question")+'][alternative][0][title]" placeholder="Nova alternativa" type="text"> </div> </div> <div class="alternativa-opcao"> <button class="nova-alternativa" data-group="'+$(this).data("group")+'" data-question="'+$(this).data("question")+'">Nova alternativa</button>  ou <button class="outra-alternativa" data-group="'+$(this).data("group")+'" data-question="'+$(this).data("question")+'">adicionar Outra</button> </div> </div> </div>';
  		}

  		wrapper_tipo.append(unica_escolha);
  		$('#research_groups_attributes_'+$(this).data("group")+'_questions_attributes_'+$(this).data("question")+'_alternatives_attributes_0_title').focus();
  	}else if($(this).val() == "lista"){
  		count = $(this).data("question")

  		lista = '<div class="row collapse"> <div class="lista large-11 columns right"> <div class="row collapse"> <div class="title large-12 columns"> <div class="row collpase"> <div class="large-1 columns"> <h6>Pergunta</h6> </div> <button class="icon-cancel question"></button><div class="large-11 columns"> <input class="string required question" data-group="'+$(this).data("group")+'" data-question="'+count+'" data-list="0" id="research_groups_attributes_'+$(this).data("group")+'_questions_attributes_'+count+'_title" name="groups['+$(this).data("group")+'][question]['+count+'][lista][0][title]" placeholder="Título da pergunta" type="text"> <input type="hidden" name="groups['+$(this).data("group")+'][question]['+count+'][lista][0][father_id]" value="'+count+'"></div> </div> <div class="row collapse"> <div class="tipo large-11 columns right"> <div class="row collpase"> <div class="large-1 columns"> <h6>Tipo</h6> </div> <div class="large-11 columns"> <select class="select required type_question" data-group="'+$(this).data("group")+'" data-question="'+count+'" id="research_groups_attributes_2_questions_attributes_1_type_question" name="groups['+$(this).data("group")+'][question]['+count+'][lista][0][type_question]"><option selected="selected" value="texto">Texto</option> <option value="unica_escolha">Unica Escolha</option> <option value="multipla_escolha">Multipla Escolha</option> </select> </div> </div> </div> </div> </div> <div class="row collpase"><button class="nova-pergunta large-12 columns text-left" data-type="lista">Nova Pergunta</button></div></div> </div> </div>';

  		wrapper_tipo.append(lista);
  		$('#research_groups_attributes_'+$(this).data("group")+'_questions_attributes_'+count+'_title').focus();
  	}

  	event.preventDefault();
  });
	
	//Remove alternative
	$(document).on("click", ".icon-cancel.alternative",function(event){
		
		group = $(this).parent().find('input').data("group");
		question = $(this).parent().find('input').data("question");
		alternative = $(this).parent().find('input').data("alternative");
		
		opcao_other = '<span class="nova-alternativa" data-group="'+group+'" data-question="'+question+'">Nova alternativa</span>  ou <span class="outra-alternativa" data-group="'+group+'" data-question="'+question+'">adicionar Outra</span>';

		opcao = '<span class="nova-alternativa" data-group="'+group+'" data-question="'+question+'">Nova alternativa</span>';

		lista = $(this).parent().parent().parent().parent().parent().parent().parent().parent().parent();
		count_lista = $(this).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().find("input.question").length-1;
		console.log(count_lista);	
		if ($(this).parent().find('input').data("other") == '1' || $(this).parent().parent().find('input[data-other="1"]').length > 0){
			console.log("as_other")
			$(this).parent().parent().parent().find(".alternativa-opcao").empty().append(opcao_other);
		}else{
			console.log("no as_other")
			$(this).parent().parent().parent().find(".alternativa-opcao").empty().append(opcao_other);
		}

		count = $(this).parent().parent().find("input").length;
		
		$(this).parent().parent().find("input").eq(count-2).focus();

		if (lista.hasClass("lista")){
			$(this).parent().hide().append('<input class="hidden" name="groups['+group+'][question]['+question+'][lista]['+count_lista+'][alternative]['+alternative+'][status]" type="hidden" value="remove">');
		}else{
			$(this).parent().hide().append('<input class="hidden" name="groups['+group+'][question]['+question+'][alternative]['+alternative+'][status]" type="hidden" value="remove">');
		}

		event.preventDefault();
	});

	//New Alternative
	$(document).on("click", ".nova-alternativa",function(event){

		count = $(this).parent().parent().find(".alternativas input").length;		
		group = $(this).data("group");
		question = $(this).data("question");

		lista = $(this).parent().parent().parent().parent().parent().parent().parent().parent();	

		if (lista.hasClass("lista")){
			count_lista = $(this).parent().parent().parent().parent().parent().parent().find("input.question").data("list");

			alternativa = '<div> <button class="icon-cancel alternative"></button> <input class="string required" data-alternative="'+count+'" data-other="0" data-group="'+group+'" data-question="'+question+'" id="research_groups_attributes_2_questions_attributes_5_alternatives_attributes_0_title" name="groups['+group+'][question]['+question+'][lista]['+count_lista+'][alternative]['+count+'][title]" placeholder="Nova alternativa" type="text"> </div>';
		}else{
			alternativa = '<div> <button class="icon-cancel alternative"></button> <input class="string required" data-alternative="'+count+'" data-other="0" data-group="'+group+'" data-question="'+question+'" id="research_groups_attributes_2_questions_attributes_5_alternatives_attributes_0_title" name="groups['+group+'][question]['+question+'][alternative]['+count+'][title]" placeholder="Nova alternativa" type="text"> </div>';
		}
		
		$(this).parent().parent().find(".alternativas").append(alternativa);

		$(this).parent().parent().find(".alternativas input").eq(count).focus();

		event.preventDefault();
	});

	//New Other Alternative
	$(document).on("click", ".outra-alternativa", function(event){
		count = $(this).parent().parent().find(".alternativas input").length;

		group = $(this).data("group");
		question = $(this).data("question");

		opcao = '<button class="nova-alternativa" data-group="'+group+'" data-question="'+question+'">Nova alternativa</button>';

		lista = $(this).parent().parent().parent().parent().parent().parent().parent().parent();	

		if (lista.hasClass("lista")){
			count_lista = $(this).parent().parent().parent().parent().parent().parent().find("input.question").data("list")
			console.log(count_lista);

			alternativa = '<div> <button class="icon-cancel alternative"></button> <input class="string required" data-alternative="'+count+'" data-other="1" id="research_groups_attributes_1_questions_attributes_6_alternatives_attributes_1_title" name="groups['+group+'][question]['+question+'][lista]['+count_lista+'][alternative]['+count+'][title]" placeholder="Outra(o)" type="text"> <input class="hidden" id="research_groups_attributes_1_questions_attributes_6_alternatives_attributes_1_as_other" name="groups['+group+'][question]['+question+'][lista]['+count_lista+'][alternative]['+count+'][as_other]" type="hidden" value="1"> </div>';
		}else{
			alternativa = '<div> <button class="icon-cancel alternative"></button> <input class="string required" data-alternative="'+count+'" data-other="1" id="research_groups_attributes_1_questions_attributes_6_alternatives_attributes_1_title" name="groups['+group+'][question]['+question+'][alternative]['+count+'][title]" placeholder="Outra(o)" type="text"> <input class="hidden" id="research_groups_attributes_1_questions_attributes_6_alternatives_attributes_1_as_other" name="groups['+group+'][question]['+question+'][alternative]['+count+'][as_other]" type="hidden" value="1"> </div>';
		}

		$(this).parent().parent().find(".alternativas").append(alternativa);

		$(this).parent().parent().find(".alternativas input").eq(count).focus();		

		$(this).parent().empty().append(opcao);

		event.preventDefault();
	});

	//Remove Question
	$(document).on("click", ".icon-cancel.question", function(event){
		$(this).parent().parent().hide();
		group = $(this).parent().parent().find("input.question:first").data("group")
		question = $(this).parent().parent().find("input.question:first").data("question")
		list = $(this).parent().parent().find("input.question:first").data("list")

		if ($(this).parent().parent().parent().parent().hasClass("lista")){
			$(this).parent().parent().append('<input class="hidden" name="groups['+group+'][question]['+question+'][lista]['+list+'][status]" type="hidden" value="remove">')
		}else{
			$(this).parent().parent().append('<input class="hidden" name="groups['+group+'][question]['+question+'][status]" type="hidden" value="remove">')
		}
		event.preventDefault();
	});

	//New Question
	$(document).on("click", ".nova-pergunta", function(event){
		group = parseInt($("dd.active a").text())-1;
		question = $(".content.active input.question").length;
		question_list = $(this).parent().parent().parent().find("input.question").length;
		wrapper_question = $(".content.active .title").not($(".lista .title")).length;		
		
		if ($(this).data("type") == 'lista' || $(this).data("type") != undefined){
			
			question_father = $(this).parent().parent().find(".title input:first").data("question");
			question = $(this).parent().parent().find(".title input:first").data("question");
			
			content_lista = '<div class="title large-12 columns"> <div class="row collpase"> <div class="large-1 columns"> <h6>Pergunta</h6> </div> <button class="icon-cancel question"></button><div class="large-11 columns"> <input class="string required question" data-group="'+group+'" data-question="'+question+'" data-list="'+question_list+'" id="research_groups_attributes_0_questions_attributes_0_title" name="groups['+group+'][question]['+question+'][lista]['+question_list+'][title]" placeholder="Título da pergunta" type="text"> <input type="hidden" name="groups['+group+'][question]['+question+'][lista]['+question_list+'][father_id]" value="'+question_father+'"></div> </div> <div class="row collapse"> <div class="tipo large-11 columns right"> <div class="row collpase"> <div class="large-1 columns"> <h6>Tipo</h6> </div> <div class="large-11 columns"> <select class="select required type_question" data-group="'+group+'" data-question="'+question+'" id="research_groups_attributes_0_questions_attributes_0_type_question" name="groups['+group+'][question]['+question+'][lista]['+question_list+'][type_question]"><option value="texto">Texto</option> <option value="unica_escolha">Unica Escolha</option> <option value="multipla_escolha">Multipla Escolha</option> </select> </div> </div> </div> </div> </div>';

			$(this).parent().parent().find(".title").eq(question_list-1).after(content_lista);
			$(this).parent().parent().find(".title").eq(question_list).find("input").focus();
		}else{

			content = '<div class="title large-12 columns"> <div class="row collpase"> <div class="large-1 columns"> <h6>Pergunta</h6> </div> <button class="icon-cancel question"></button> <div class="large-11 columns"> <input class="string required question" data-group="'+group+'" data-question="'+question+'" id="research_groups_attributes_0_questions_attributes_0_title" name="groups['+group+'][question]['+question+'][title]" placeholder="Título da pergunta" type="text"> </div> </div> <div class="row collapse"> <div class="tipo large-11 columns right"> <div class="row collpase"> <div class="large-1 columns"> <h6>Tipo</h6> </div> <div class="large-11 columns"> <select class="select required type_question" data-group="'+group+'" data-question="'+question+'" id="research_groups_attributes_0_questions_attributes_0_type_question" name="groups['+group+'][question]['+question+'][type_question]"><option value="texto">Texto</option> <option value="unica_escolha">Unica Escolha</option> <option value="multipla_escolha">Multipla Escolha</option> <option value="lista">Lista</option></select> </div> </div> </div> </div> </div>';

			children = $(".content.active").children().length;
			$(".content.active").children().eq(children-1).before(content);
			$(".content.active .title").not($(".lista .title")).eq(wrapper_question).find("input").focus();
		}
		event.preventDefault();
	});	

	//Remove answer list
	$(document).on("click", "span.icon-cancel.answer-list", function(event){		
		tr_count = $(this).parent().parent().index()
		
		$.each($(this).parent().parent().find("td"), function(question){
			data = $(this).find('input:first')
			$(this).append('<input name="question['+data.data("question")+']['+tr_count+'][status]" type="hidden" value="remove">')
		});

		$(this).parent().parent().hide();

	});

	//Insert new answer list
	$("table.lista tbody tr:last-child").click(function(){
		body = $(this).parent();
		clone = '<tr>'+body.find("tr:first").html()+'</tr>';
		tr_count = body.find("tr").length-1;

		remove_anwser = '<span class="icon-cancel answer-list hide"></span>'

		$(this).before(clone);

		$.each($(this).parent().find("tr").eq(tr_count).find("td"), function(index){
			if (index == 0){
				if ($(this).find("div:first").length > 0){
					$(this).find("div:first").before(remove_anwser);
				}else{
					$(this).find("input:first").before(remove_anwser);
				}
			}

			$.each($(this).find("input"), function(question){
				
				if ($(this).attr("type") == 'text'){
					$(this).attr("name", 'question['+$(this).data("question")+']['+tr_count+'][value]')
				}else if ($(this).attr("type") == 'radio'){
					$(this).parent().attr("for", "answer_id_"+tr_count+"_"+$(this).val())
					$(this).attr("id", "answer_id_"+tr_count+"_"+$(this).val())
					$(this).attr("name", 'question['+$(this).data("question")+']['+tr_count+'][alternative]')
				}else if ($(this).attr("type") == 'hidden'){
					$(this).attr("id", "answer_id_"+tr_count+"_"+$(this).val())
					$(this).attr("name", 'question['+$(this).data("question")+']['+tr_count+'][alternative]')
				}else if ($(this).attr("type") == 'checkbox'){
					$(this).parent().attr("for", "answer_id_"+tr_count+"_"+$(this).val())
					$(this).attr("id", "answer_id_"+tr_count+"_"+$(this).val())
					$(this).attr("name", 'question['+$(this).data("question")+']['+tr_count+'][alternative][]')
				}
			});
		});
	});

	//Insert remove answer list
	$("table.lista").each(function(table){
		table_tr_length = $(this).find("tbody tr").length
		$(this).find("tbody tr").each(function(tr){
			if (table_tr_length > 3 && tr > 1){

			}	
		});
	});

  //Keypress events
  $(".simple_form edit_survey").keypress(function(e){
    if (e.witch == 13) return false;
  });

  $(document).on("keydown", function (e) {
    if (e.which === 8 && !$(e.target).is("input, textarea")) {
      e.preventDefault();
    }
  });
});