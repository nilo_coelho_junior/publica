class Question < ActiveRecord::Base
  enum type_question: {texto: 0, unica_escolha: 1, multipla_escolha: 2, lista: 3}
  
  belongs_to :group
  has_many :alternatives, dependent: :destroy
  has_many :answers, dependent: :destroy

  has_many :question_lists, class_name: "Question", foreign_key: "father_id", dependent: :destroy
  belongs_to :father, class_name: "Question"

  validates :title, :type_question, presence: true

  accepts_nested_attributes_for :group, :alternatives
end
