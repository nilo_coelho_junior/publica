class Ability
  include CanCan::Ability

  def initialize(user)
    
    case user.role
        when "administrador"
          can :manage, :all
        when "coordenador"
          can :index, Event if Event.for_coordinator(user.id, "")
          can :manage, Research if Research.for_coordinator(Event, user.id, "")
          can :manage, [Identifier, Survey, Answer, Group, Question, Admin::QuestionListsController, Admin::ImportSurveysController, Admin::SurveyParticipantsController, Alternative, Admin::AlternativesListsController]
          can [:edit, :show, :destroy], User, :id => user.id
        when "pesquisador"
          can :index, Event if Event.tasker_with_surveys(user.id, "")
          can :index, Research if Research.tasker_with_surveys(Event, user.id, "")
          can :index, Survey, :participant_id => user.id
          can :manage, Answer 
        else
    end

  end
end
