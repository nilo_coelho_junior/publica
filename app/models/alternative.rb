class Alternative < ActiveRecord::Base
  belongs_to :question
  has_many :answers, dependent: :destroy

  validates :title, presence: true
end
