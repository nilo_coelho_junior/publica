class Survey < ActiveRecord::Base
  belongs_to :research
  
  belongs_to :participant, class_name: "User"

  has_many :answers, dependent: :destroy

  has_many :survey_identifiers, dependent: :destroy
  has_many :identifiers, through: :survey_identifiers

  belongs_to :author, class_name: "User"

  paginates_per 15

  accepts_nested_attributes_for :answers
  accepts_nested_attributes_for :survey_identifiers
  accepts_nested_attributes_for :identifiers

  validates :number_survey, uniqueness: {scope: :research_id, message: "Questionário ja cadastrado"}, presence: true, allow_blank: false

  def to_param
  	"#{number_survey}"
  end

  def self.by_research(research_id)
    where("research_id = ?", research_id)
  end

  def self.for_tasker(research_id, user)
    where("research_id = ? and participant_id = ?", research_id, user)
  end

  def self.filter(de, ate)
    if first.number_survey.to_i != 0
      where("(number_survey)::INT between ? and ?", research_id, user, number_survey, de, ate)
    else
      where("research_id = ? and participant_id: ? and number_survey between ? and ?", research_id, user, number_survey, de, ate)
    end
  end

  def self.search(search)
    if first.number_survey.to_i !=0
      where("(number_survey)::INT = ?", search)
    else
      where("number_survey like ?", "%#{search}%")
    end
  end
end