class Answer < ActiveRecord::Base
	belongs_to :question
	belongs_to :alternative
	belongs_to :survey

	has_many :alternatives, through: :question 

	accepts_nested_attributes_for :question
	accepts_nested_attributes_for :alternatives
	
end
