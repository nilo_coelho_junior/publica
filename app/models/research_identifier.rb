class ResearchIdentifier < ActiveRecord::Base
	belongs_to :research
	belongs_to :identifier
end
