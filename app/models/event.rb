class Event < ActiveRecord::Base
	enum status: {desativado: 0, ativado: 1}
	belongs_to :author, class_name: "User"
	has_many :researches, dependent: :destroy

	has_many :identifiers

	paginates_per 6
	
	accepts_nested_attributes_for :researches

	validates :title, presence: true

	
	def self.active
		where(status: true)
	end

	def self.for_coordinator(user, search)
		if search.nil? or search.blank?
			where('status = 1 and events.id in (select event_id from researches where status = 1 and researches.id in (select research_id from participants where research_id = researches.id and user_id = ?))', user)
		else
			where('LOWER(title) like LOWER(?) and status = 1 and events.id in (select event_id from researches where status = 1 and researches.id in (select research_id from participants where research_id = researches.id and user_id = ?))', "%#{search}%", user)
		end
	end

	def self.tasker_with_surveys(user, search)
		if !search.nil? and !search.blank?
			where('status = 1 and events.id in (select event_id from researches where status = 1 and researches.id in (select research_id from surveys where surveys.research_id = researches.id and participant_id = ?))', user)
		else
			where('LOWER(title) like LOWER(?) and status = 1 and events.id in (select event_id from researches where status = 1 and researches.id in (select research_id from surveys where surveys.research_id = researches.id and participant_id = ?))', "%#{search}%", user)
		end
	end

	def self.search(search)
    where("LOWER(title) like LOWER(?)", "%#{search}%")
	end
end
