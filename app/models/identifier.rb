class Identifier < ActiveRecord::Base

	has_many :research_identifiers, dependent: :destroy
	has_many :researches, through: :research_identifiers

	has_many :survey_identifiers, dependent: :destroy
	has_many :surveys, through: :survey_identifiers

	validates :name, presence: true

	accepts_nested_attributes_for :researches
	accepts_nested_attributes_for :surveys, :survey_identifiers

	validates :name, presence: true

	def self.search()
		where("LOWER(title) like LOWER(%#{params[:search]}%)")
	end

end
