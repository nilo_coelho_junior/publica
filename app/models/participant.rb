class Participant < ActiveRecord::Base
	belongs_to :research
	belongs_to :user

	accepts_nested_attributes_for :research
end
