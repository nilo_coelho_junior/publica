class Research < ActiveRecord::Base
	enum status: {desativado: 0, ativado: 1}

	has_many :participants, dependent: :destroy
	has_many :users, through: :participants

	has_many :research_identifiers, dependent: :destroy
	has_many :identifiers, through: :research_identifiers

	belongs_to :event
	belongs_to :author, class_name: "User"

	has_many :groups, ->{order :id}, dependent: :destroy
	has_many :surveys, dependent: :destroy

	accepts_nested_attributes_for :users, :identifiers, :groups	

	validates :title, presence: true

	def self.with_identifiers(event_id)
		where('event_id = ? and exists (SELECT (research_id) from research_identifiers)', event_id)
	end

	def self.for_coordinator(event_id, user, search)
		if !search.nil? and !search.blank?
			where('event_id = ? and status = 1 and exists (select * from participants where research_id = researches.id and user_id = ?)', event_id, user)
		else
			where('event_id = ? and status = 1 and exists (select * from participants where research_id = researches.id and user_id = ?) and LOWER(title) like LOWER(?)', event_id, user, "%#{search}%")
		end
	end

	def self.tasker_with_surveys(event_id, user, search)
		if !search.nil? and !search.blank?
			where('event_id = ? and status = 1 and exists (select * from surveys where surveys.research_id = researches.id and participant_id = ?)', event_id, user)
		else
			where('event_id = ? and status = 1 and exists (select * from surveys where surveys.research_id = researches.id and participant_id = ?) and LOWER(title) like LOWER(?)', event_id, user, "%#{search}%")
		end
	end

	def self.search(event_id, search)
		where("event_id = ? and LOWER(title) like LOWER(?)", event_id, "%#{search}%")
	end
end
