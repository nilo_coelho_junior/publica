class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  enum role: {administrador: 0, coordenador: 1, pesquisador: 2, banido: 3}
  
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  #has_many :researchers
  has_many :surveys
  has_many :participants, inverse_of: :user, dependent: :destroy
  has_many :researchers, through: :participants

  validates :email, :name, :role, presence: true

  accepts_nested_attributes_for :researchers

  def self.coordinators
    where(role: 1).order(:name)
  end

  def self.survey_taskers
    where(role: 2).order(:name)
  end

  def self.whithout_participant(research_id)
    survey_taskers.where("NOT EXISTS (select * from surveys where surveys.participant_id = users.id and research_id = ?)", research_id)
  end

end
