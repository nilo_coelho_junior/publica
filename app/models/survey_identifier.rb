class SurveyIdentifier < ActiveRecord::Base
	belongs_to :identifier
	belongs_to :survey

	validates :value, uniqueness: {scope: [:identifier, :survey], message: "Questionário já cadastrado"}
end
