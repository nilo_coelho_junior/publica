module ApplicationHelper
	def get_page(object, collection)
		(collection.index(object).to_f/collection.default_per_page).ceil
	end
end
