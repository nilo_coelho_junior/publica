require 'rails_helper'

describe Event do
  describe "#Events" do

    it "Title present" do
      event = Event.create(title: "teste", description: "teste")
      event2 = Event.create
      expect(event.valid?).to eq(true)
      expect(event2.valid?).to eq(false)
    end

    it "Active event by default" do
      event = Event.create(title: "teste", description: "teste", status: 1)
      event2 = Event.create(title: "teste", description: "teste")

      expect(event.status).to eq("ativado")
      expect(event2.status).to eq("ativado")
    end

    it "Active Events" do
      event = Event.create(title: "teste")
      event2 = Event.create(title: "teste2", status: 0)

      expect(Event.active.pluck(:title)).to match_array(["teste"])
    end

    it "Search event" do
      event = Event.create(title: "teste")
      event2 = Event.create(title: "teste2")
      event3 = Event.create(title: "fase teste")
      event4 = Event.create(title: "teste fase")

      expect(Event.search("teste").order("created_at").pluck(:title)).to match_array(["teste", "teste2", "fase teste", "teste fase"])
    end

    it "Disable event" do
      event = Event.create(title: "teste")

      event.desativado!
      expect(event.status).to eq("desativado")
    end


  end
end