require "rails_helper"

describe "user" do 

  subject(:user) do
    user = User.new(email: "teste@email.com", name: "Teste", role: 0, password: "12345678")
  end

  it "user valid" do
    expect(user.valid?).to eq(true)
  end

  context "ActiveModel validations" do
    it "email presence" do 
      expect(user.valid?).to be_invalid
    end
  end

  context "scopes" do
    it "Coordinators" do
      user = User.create(email: "teste@email.com", name: "Teste", role: 0, password: "12345678")
      user2 = User.create(email: "teste2@email.com", name: "Teste2", role: 1, password: "12345678")

      expect(User.coordinators.pluck(:name)).to match_array(["Teste2"]) 
    end

    it "Survey Taskers" do
      user = User.create(email: "teste@email.com", name: "Teste", role: 0, password: "12345678")
      user2 = User.create(email: "teste2@email.com", name: "Teste2", role: 3, password: "12345678")
      
      expect(User.survey_taskers.pluck(:name)).to match_array(["Teste2"]) 
    end

    it "Whitout participants" do
      user = User.create(email: "teste@email.com", name: "Teste", role: 0, password: "12345678")
      event = Event.create(title: "Teste")
      research = Research.create(title: "research teste", event: event)
      identifier = Identifier.create(name: "numero")
      survey = Survey.create()
    end
  end
end