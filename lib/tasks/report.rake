namespace :report do

	desc "Count alternatives from generete report for research"

	task :count_alternatives, [:research] => :environment do |t, args|
		research = Research.find(args.research)

		research.groups.each_with_index{|group, key|
			group.questions.where(father_id: nil).where.not(type_question: "texto").order("id").each_with_index{|question, key|
				if question.type_question == "unica_escolha" || question.type_question == "multipla_escolha"
					# p "-----------------------------------------------------------------"
					# p "QUESTION #{question.id} - #{question.type_question}"
					# p question.alternatives.ids
					question.alternatives.each_with_index{|alternative, index|
						count = 0
						# p "ALTERNATIVE #{alternative.id}"
						
						question_alternatives = question.alternatives.ids.map{|v| v.to_s}

						question.answers.each_with_index{|answer, index|
							
							# p "ANSWERS #{answer.objective}"
							if alternative.as_other == true
								other = answer.objective.reject{|v| v.empty?} - question_alternatives
								count += other.size
							else
								count += answer.objective.count(alternative.id.to_s)
							end
							# p "COUNT #{count}"
						}
						alternative.update(count: count)
						
					}
					# p "-----------------------------------------------------------------"	
				elsif question.type_question == "lista"
					# p "QUESTION LIST #{question.id}"
					group.questions.where(father_id: question.id).where.not(type_question: 0).order("id").each{|question_children|
						# p "QUESTION CHILD #{question_children.id}"
						question_children.alternatives.each_with_index{|alternative, index|
							count = 0
							# p "ALTERNATIVE #{alternative.id}"
							question_children.answers.each_with_index{|answer, index|
								# p "ANSWERS #{answer.objective}"
								count += answer.objective.to_h.values.count(alternative.id.to_s)
							}
							# p "COUNT #{count}"
							alternative.update(count: count)
						}
					}
				end
			}
		}
	end

	task import_answers: :environment do |t|		

		CSV.foreach("public/answers.csv", headers: true) do |row|
			
			if !Question.where(id: row[1].to_i).blank?

				question = Question.find(row[1].to_i)

				if question.father_id != nil
					
					if question.type_question == "texto"
						

						row[3].slice(1, row[3].size-2).tr("\"", "").strip.split(",").each{|value|
							if !value.blank?
								Answer.create(question_id: row[1], survey_id: row[6], alternative_id: question.alternatives.first.id, value: value, created_at: row[4], updated_at: row[5])
							end
						}
					else
						
						row[2].scan(/[\w+][\w+][\w+]/).each{|alternative|
							Answer.create(question_id: row[1], survey_id: row[6], alternative_id: alternative, created_at: row[4], updated_at: row[5])	
						}
					end

				elsif question.type_question == "texto"
					
					answer = Answer.new(question_id: row[1], survey_id: row[6], alternative_id: question.alternatives.first.id, created_at: row[4], updated_at: row[5])
					
					if !row[3].nil?
			  		if !row[3].scan(/\w+/).join("").empty?
			  			answer.value = row[3].scan(/\w+/).join("")
			  			answer
			  		end
			  	end

			  	answer.save
				else
					
					row[2].scan(/\w+/).each{|alternative|

				  	answer = Answer.new(question_id: row[1], survey_id: row[6], alternative_id: alternative, created_at: row[4], updated_at: row[5])

				  	if !row[3].nil?
				  		if !row[3].scan(/\w+/).join("").empty?
				  			answer.value = row[3].scan(/\w+/).join("")
				  			answer
				  		end
				  	end
				  	answer.save
					}
				end

			end
		end
	end
end