module Report
	class Generate
		def report(research)
			query = "select b.question_id, a.alternative_title, count(b.objective_id) from (select researches.title as research, groups.title as group_title, questions.id as question_id, questions.title as question_title, alternatives.id as alternative_id, alternatives.title as alternative_title from researches, groups, questions, alternatives where researches.id = #{research.id} and groups.research_id = #{research.id} and questions.group_id = groups.id and alternatives.question_id = questions.id order by questions.id) as a, (select answers.question_id as question_id, answers.objective as objective_id from surveys, answers where surveys.research_id = #{research.id} and answers.survey_id = surveys.id order by answers.question_id) as b where a.question_id = b.question_id and a.alternative_id::text = ANY (b.objective_id) group by b.question_id, a.alternative_title order by b.question_id"

      		@count = ActiveRecord::Base.connection.execute(query).to_a

      		@report = Hash.new
		    groups = Array.new
		    questions = Array.new
		    respostas = Array.new

		    @number = 10000.times.map{Random.rand(1...10000) }

		    research.groups.each_with_index do |group, key|
		      groups << ["grupo" => group.title]

		      group.questions.each_with_index do |question, key2|

		        questions[key2].blank? ? questions << ["pergunta" => question.title, "type_question" => question.type_question, "id" => @number.sample] : questions[key2] << ["pergunta" => question.title, "type_question" => question.type_question, "id" => @number.sample]

		        @count.each_with_index do |value, key3|
		          if value["question_id"] == question.id.to_s
		            respostas[key3].blank? ? respostas << ["alternativa" => value["alternative_title"], "count" => value["count"]] : respostas[key3] << ["alternativa" => value["alternative_title"], "count" => value["count"]]
		          end
		        end
		        p "--------------#{respostas}"
		        questions[key2].blank? ? '' : questions[key2]<< respostas
		        respostas = Array.new
		      end
		      groups << questions
		      questions = Array.new
		    end
		    @report = groups
		    p "--------------------#{@report}"
		    @report
		end

		def count(research, start, end)
			research.groups.each_with_index{|group, key|
				group.questions.where(father_id: nil).where.not(type_question: "texto").order("id").each_with_index{|question, key|
					if question.type_question == "unica_escolha" || question.type_question == "multipla_escolha"
						# p "-----------------------------------------------------------------"
						# p "QUESTION #{question.id} - #{question.type_question}"
						# p question.alternatives.ids
						question.alternatives.each_with_index{|alternative, index|
							count = 0
							# p "ALTERNATIVE #{alternative.id}"
							
							question_alternatives = question.alternatives.ids.map{|v| v.to_s}

							question.answers.each_with_index{|answer, index|
								# p "ANSWERS #{answer.objective}"
								if alternative.as_other == true
									other = answer.objective.reject{|v| v.empty?} - question_alternatives
									count += other.size
								else
									count += answer.objective.count(alternative.id.to_s)
								end
								# p "COUNT #{count}"
							}
							alternative.update(count: count)
							
						}
						# p "-----------------------------------------------------------------"	
					elsif question.type_question == "lista"
						# p "QUESTION LIST #{question.id}"
						group.questions.where(father_id: question.id).where.not(type_question: 0).order("id").each{|question_children|
							# p "QUESTION CHILD #{question_children.id}"
							question_children.alternatives.each_with_index{|alternative, index|
								count = 0
								# p "ALTERNATIVE #{alternative.id}"
								question_children.answers.each_with_index{|answer, index|
									# p "ANSWERS #{answer.objective}"
									count += answer.objective.to_h.values.count(alternative.id.to_s)
								}
								# p "COUNT #{count}"
								alternative.update(count: count)
							}
						}
					end
				}
			}
		end
	end 
end